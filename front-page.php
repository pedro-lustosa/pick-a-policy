<?php require "functions.php"; ?>
<!DOCTYPE html>
<html lang="en-US">
  <head>
    <meta charset="utf-8">
    <meta name="author" content="This theme was created by the brazilian company DR Estúdio &ndash; https://danielrothier.com">
    <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1">

    <title>Picka Policy</title>

    <link rel="icon" href="multimedia/images/x-small/xs-favicon-1.jpg">

    <link rel="stylesheet" href="styles/front-page/front-page-min.css">
    <script defer src="scripts/front-page/front-page-min.js" type="module"></script>
  </head>
  <body>
    <?php get_top_header( array( 'Magazine' => '#', 'About' => 'content/about-us.php', 'Contact' => '#' ) ) ?>
    <main id="top-content">
      <section id="intro">
        <header class="heading-block">
          <div class="content-container">
            <div class="title-container">
              <h1 class="title">
                <span class="side-line">Insure Your</span>
                <span class="display-line">Family</span>
                <span class="side-line">Ensure your</span>
                <span class="display-line">Future</span>
                <span class="side-line">Estabilish a</span>
                <span class="display-line">Legacy</span>
              </h1>
            </div>
          </div>
        </header>
        <main class="form-block">
          <div class="content-container">
            <form name="quote-form-1" action="" method="post" autocomplete="off">
              <main class="form-body">
                <fieldset class="zip-field">
                  <label for="zip-1">Zip Code</label>
                  <input id="zip-1" name="zip" type="text" placeholder="Enter your zip code here">
                </fieldset>
                <button type="submit" name="quote">Get a Quote Now</button>
              </main>
              <footer class="form-footer">
                <p class="message">Need to see more? <a href="#">Start here.</a></p>
              </footer>
            </form>
            <figure class="video-image">
              <picture class="background-image">
                <source srcset="multimedia/images/medium/m-family-1.webp" type="image/webp">
                <img src="multimedia/images/medium/m-family-1.jpg" alt="Video.">
              </picture>
              <div class="image-components">
                <picture class="play-button">
                  <img src="multimedia/images/small/s-play-1.png" alt="Play the video.">
                </picture>
                <figcaption class="description">
                  <picture>
                    <img src="multimedia/images/small/s-action-1.png" alt="Click here and watch the video">
                  </picture>
                </figcaption>
              </div>
              </figure>
            </div>
          </div>
        </main>
        <footer class="footer-block">
          <div class="content-container">
            <ul class="companies-list">
              <li><img src="multimedia/images/small/s-aig-1.png" alt="AIG"></li>
              <li><img src="multimedia/images/small/s-ameritas-1.png" alt="Ameritas"></li>
              <li><img src="multimedia/images/small/s-principal-1.png" alt="Principal"></li>
              <li><img src="multimedia/images/small/s-protective-1.png" alt="Protective"></li>
              <li><img src="multimedia/images/small/s-prudential-1.png" alt="Prudential"></li>
              <li><img src="multimedia/images/small/s-principal-1.png" alt="Principal"></li>
              <li><img src="multimedia/images/small/s-principal-1.png" alt="Principal"></li>
              <li><img src="multimedia/images/small/s-principal-1.png" alt="Principal"></li>
              <li><img src="multimedia/images/small/s-principal-1.png" alt="Principal"></li>
              <li><img src="multimedia/images/small/s-principal-1.png" alt="Principal"></li>
              <li><img src="multimedia/images/small/s-principal-1.png" alt="Principal"></li>
              <li><img src="multimedia/images/small/s-principal-1.png" alt="Principal"></li>
              <li><img src="multimedia/images/small/s-principal-1.png" alt="Principal"></li>
              <li><img src="multimedia/images/small/s-principal-1.png" alt="Principal"></li>
              <li><img src="multimedia/images/small/s-principal-1.png" alt="Principal"></li>
              <li><img src="multimedia/images/small/s-principal-1.png" alt="Principal"></li>
              <li><img src="multimedia/images/small/s-principal-1.png" alt="Principal"></li>
              <li><img src="multimedia/images/small/s-principal-1.png" alt="Principal"></li>
              <li><img src="multimedia/images/small/s-principal-1.png" alt="Principal"></li>
              <li><img src="multimedia/images/small/s-principal-1.png" alt="Principal"></li>
              <li><img src="multimedia/images/small/s-principal-1.png" alt="Principal"></li>
              <li><img src="multimedia/images/small/s-principal-1.png" alt="Principal"></li>
              <li><img src="multimedia/images/small/s-principal-1.png" alt="Principal"></li>
              <li><img src="multimedia/images/small/s-principal-1.png" alt="Principal"></li>
              <li><img src="multimedia/images/small/s-principal-1.png" alt="Principal"></li>
              <li><img src="multimedia/images/small/s-principal-1.png" alt="Principal"></li>
              <li><img src="multimedia/images/small/s-principal-1.png" alt="Principal"></li>
              <li><img src="multimedia/images/small/s-principal-1.png" alt="Principal"></li>
              <li><img src="multimedia/images/small/s-principal-1.png" alt="Principal"></li>
              <li><img src="multimedia/images/small/s-principal-1.png" alt="Principal"></li>
              <li><img src="multimedia/images/small/s-principal-1.png" alt="Principal"></li>
              <li><img src="multimedia/images/small/s-principal-1.png" alt="Principal"></li>
              <li><img src="multimedia/images/small/s-principal-1.png" alt="Principal"></li>
              <li><img src="multimedia/images/small/s-principal-1.png" alt="Principal"></li>
              <li><img src="multimedia/images/small/s-principal-1.png" alt="Principal"></li>
              <li><img src="multimedia/images/small/s-principal-1.png" alt="Principal"></li>
              <li><img src="multimedia/images/small/s-principal-1.png" alt="Principal"></li>
              <li><img src="multimedia/images/small/s-principal-1.png" alt="Principal"></li>
              <li><img src="multimedia/images/small/s-principal-1.png" alt="Principal"></li>
              <li><img src="multimedia/images/small/s-principal-1.png" alt="Principal"></li>
              <li><img src="multimedia/images/small/s-principal-1.png" alt="Principal"></li>
              <li><img src="multimedia/images/small/s-principal-1.png" alt="Principal"></li>
              <li><img src="multimedia/images/small/s-principal-1.png" alt="Principal"></li>
              <li><img src="multimedia/images/small/s-principal-1.png" alt="Principal"></li>
              <li><img src="multimedia/images/small/s-principal-1.png" alt="Principal"></li>
              <li><img src="multimedia/images/small/s-principal-1.png" alt="Principal"></li>
              <li><img src="multimedia/images/small/s-principal-1.png" alt="Principal"></li>
              <li><img src="multimedia/images/small/s-principal-1.png" alt="Principal"></li>
            </ul>
            <div class="displayer-button">
              <picture class="button-border">
                <img src="multimedia/images/small/s-border-1.png" alt="Show more">
              </picture>
              <div class="button-body"></div>
            </div>
          </div>
        </footer>
      </section>
      <section id="life-events">
        <div class="content-container">
          <header class="heading-container">
            <hgroup class="heading-set">
              <h2 class="title"><span class="highlight m-line">Our</span> <span class="gradient">life</span> <span class="m-gradient">events</span></h2>
              <h3 class="subtitle">Reasons for Happiness<span class="removable"> and Meaning for the Future</span></h3>
            </hgroup>
          </header>
          <div class="side-images">
            <div class="row">
              <figure class="image-container home">
                <picture class="ilustration">
                  <source srcset="multimedia/images/small/s-ilustration-7.webp" type="image/webp">
                  <img src="multimedia/images/small/s-ilustration-7.png" alt="">
                </picture>
                <figcaption class="description-container">
                  <h1 class="description">
                    <span class="line">Buying</span>
                    <span class="line">a Home</span>
                  </h1>
                </figcaption>
              </figure>
              <figure class="image-container maternity">
                <picture class="ilustration">
                  <source srcset="multimedia/images/small/s-ilustration-6.webp" type="image/webp">
                  <img src="multimedia/images/small/s-ilustration-6.png" alt="">
                </picture>
                <figcaption class="description-container">
                  <h1 class="description">
                    <span class="line">Having</span>
                    <span class="line">A Baby</span>
                  </h1>
                </figcaption>
              </figure>
              <figure class="image-container marriage">
                <picture class="ilustration">
                  <source srcset="multimedia/images/small/s-ilustration-2.webp" type="image/webp">
                  <img src="multimedia/images/small/s-ilustration-2.png" alt="">
                </picture>
                <figcaption class="description-container">
                  <h1 class="description">
                    <span class="line">Getting</span>
                    <span class="line">Married</span>
                  </h1>
                </figcaption>
              </figure>
            </div>
            <div class="row">
              <figure class="image-container aging">
                <picture class="ilustration">
                  <source srcset="multimedia/images/small/s-ilustration-3.webp" type="image/webp">
                  <img src="multimedia/images/small/s-ilustration-3.png" alt="">
                </picture>
                <figcaption class="description-container">
                  <h1 class="description">
                    <span class="line">Elderly</span>
                    <span class="line">Parents</span>
                  </h1>
                </figcaption>
              </figure>
              <figure class="image-container legacy">
                <picture class="ilustration">
                  <source srcset="multimedia/images/small/s-ilustration-1.webp" type="image/webp">
                  <img src="multimedia/images/small/s-ilustration-1.png" alt="">
                </picture>
                <figcaption class="description-container">
                  <h1 class="description">
                    <span class="line">Creating</span>
                    <span class="line">a Legacy</span>
                  </h1>
                </figcaption>
              </figure>
              <figure class="image-container wealth">
                <picture class="ilustration">
                  <source srcset="multimedia/images/small/s-ilustration-4.webp" type="image/webp">
                  <img src="multimedia/images/small/s-ilustration-4.png" alt="">
                </picture>
                <figcaption class="description-container">
                  <h1 class="description">
                    <span class="line">Retaining</span>
                    <span class="line">Wealth</span>
                  </h1>
                </figcaption>
              </figure>
              <figure class="image-container investment">
                <picture class="ilustration">
                  <source srcset="multimedia/images/small/s-ilustration-5.webp" type="image/webp">
                  <img src="multimedia/images/small/s-ilustration-5.png" alt="">
                </picture>
                <figcaption class="description-container">
                  <h1 class="description">
                    <span class="line">Starting</span>
                    <span class="line">A Business</span>
                  </h1>
                </figcaption>
              </figure>
            </div>
          </div>
        </div>
      </section>
      <section id="track-banner">
        <div class="banner">
          <div class="content-container">
            <div class="text-container">
              <header class="heading-container">
                <h1 class="title">
                  <span class="side-line">Our</span>
                  <span class="main-line"><span class="line">Track</span> <span class="line">Record</span></span>
                </h1>
              </header>
              <div class="text-set">
                <p class="text">
                  Quidem saepe nisi vivamus cumque tellus tincidunt, eleifend vitae lacinia mattis, asperiores illum nonummy non delectus! Pulvinar molestiae fusce arcu hymenaeos eiusmod.
                </p>
              </div>
              <div class="button-container">
                <button type="button" name="quote">Get a quote now</button>
              </div>
            </div>
            <picture class="side-image">
              <img src="multimedia/images/medium/m-display-text-1.png" alt="$1 Billion">
            </picture>
          </div>
        </div>
      </section>
      <section id="testimonials">
        <div class="content-container">
          <header class="heading-container">
            <hgroup class="heading-set">
              <h2 class="title"><span class="highlight">Te</span><span class="gradient">st</span>imonials</h2>
              <h3 class="subtitle"><span class="removable">Happy </span>People Talk<span class="removable">ing</span> About Us</h3>
            </hgroup>
            <nav class="testimonials-nav">
              <picture class="trustpilot">
                <img src="multimedia/images/small/s-logo-2.png" alt="Trustpilot logo">
              </picture>
              <div class="arrows-set">
                <picture class="previous larr">
                  <img src="multimedia/images/x-small/xs-larr-1.png" alt="Previous">
                </picture>
                <picture class="next rarr">
                  <img src="multimedia/images/x-small/xs-larr-1.png" alt="Next">
                </picture>
              </div>
            </nav>
          </header>
          <section class="testimonial-content">
            <picture class="slideshow-image">
              <source srcset="multimedia/images/medium/m-people-1.webp" type="image/webp">
              <img src="multimedia/images/medium/m-people-1.jpg" alt="Brett D.">
            </picture>
            <div class="slideshow-text">
              <header class="heading-container">
                <h3 class="title">And they actually enjoyed it.</h3>
              </header>
              <blockquote class="text-set">
                <p class="quote">Awesome service! Great communication, easy to navigate website, even better pricing. Very happy.</p>
              </blockquote>
              <footer class="footer-container">
                <span class="name">Brett D.</span>
              </footer>
            </div>
          </section>
          <div hidden class="slideshow-content">
            <ul class="images">
              <li>
                <picture class="slideshow-image">
                  <source srcset="multimedia/images/medium/m-people-1.webp" type="image/webp">
                  <img src="multimedia/images/medium/m-people-1.jpg" alt="Brett D.">
                </picture>
              </li>
              <li>
                <picture class="slideshow-image">
                  <img data-src="multimedia/images/medium/m-dolorum-1.jpg" alt="">
                </picture>
              </li>
              <li>
                <picture class="slideshow-image">
                  <img data-src="multimedia/images/medium/m-etiam-1.jpg" alt="">
                </picture>
              </li>
            </ul>
            <ul class="texts">
              <li>
                <div class="slideshow-text">
                  <header class="heading-container">
                    <h3 class="title">And they actually enjoyed it.</h3>
                  </header>
                  <blockquote class="text-set">
                    <p class="quote">Awesome service! Great communication, easy to navigate website, even better pricing. Very happy.</p>
                  </blockquote>
                  <footer class="footer-container">
                    <span class="name">Brett D.</span>
                  </footer>
                </div>
              </li>
              <li>
                <div class="slideshow-text">
                  <header class="heading-container">
                    <h3 class="title">Nunc in turpis commodo, pharetra ligula at, dictum urna!</h3>
                  </header>
                  <blockquote class="text-set">
                    <p class="quote">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse rutrum justo in suscipit convallis.</p>
                  </blockquote>
                  <footer class="footer-container">
                    <span class="name">Sample 1</span>
                  </footer>
                </div>
              </li>
              <li>
                <div class="slideshow-text">
                  <header class="heading-container">
                    <h3 class="title">Phasellus lacinia ante vel dui mollis porttitor.</h3>
                  </header>
                  <blockquote class="text-set">
                    <p class="quote">Aenean nec quam id ex eleifend aliquet vitae a erat. Duis egestas velit ultricies neque varius gravida. Praesent tellus enim, ornare id sapien mollis, rhoncus feugiat purus.</p>
                  </blockquote>
                  <footer class="footer-container">
                    <span class="name">Sample 2</span>
                  </footer>
                </div>
              </li>
            </ul>
          </div>
        </div>
        <picture class="smudge">
          <source srcset="multimedia/images/large/l-smudge-1.webp" type="image/webp">
          <img src="multimedia/images/large/l-smudge-1.png" alt="">
        </picture>
      </section>
      <section id="picklife">
        <div class="content-container">
          <header class="heading-container">
            <hgroup class="heading-set">
              <h2 class="title"><span class="highlight">Pi</span><span class="gradient">ck</span>life</h2>
              <h3 class="subtitle">Your Lifestyle Magazine</h3>
            </hgroup>
          </header>
          <table class="articles-block">
            <tbody class="articles-group">
              <tr>
                <td rowspan="2" colspan="2" class="content-box dolorum featured">
                  <article>
                    <div class="info-box">
                      <div class="description-container">
                        <header class="heading-container">
                          <time datetime="2018-04-6">Pick Life, April 6th, 2018</time>
                          <h3 class="title">
                            Dolorum Aspernatur Natoque Conubia Occaecat
                          </h3>
                        </header>
                        <p class="description">
                          Accusamus metus viverra inceptos omnis tempus suscipit, sollicitudin, sint perferendis dolorem incididunt occaecati viverra excepturi. Curabitur et sem vulputate, accumsan nunc eget, dictum massa.
                        </p>
                      </div>
                      <div class="know-more"></div>
                    </div>
                  </article>
                </td>
                <td class="content-box etiam featured">
                  <article>
                    <div class="info-box">
                      <div class="description-container">
                        <header class="heading-container">
                          <time datetime="2018-03-3">Pick Life, March 3th, 2018</time>
                          <h3 class="title">
                            Etiam justo quam, ultricies non feugiat ne, eleifend id tellus.
                          </h3>
                        </header>
                        <p class="description">
                          Donec ut urna eu diam tincidunt dapibus. Integer sit amet justo egestas, porta risus a, malesuada ante. Cras mattis justo id erat vulputate, in tempus diam dictum.
                        </p>
                      </div>
                      <div class="know-more"></div>
                    </div>
                  </article>
                </td>
              </tr>
              <tr>
                <td class="content-box morbi">
                  <article>
                    <div class="info-box">
                      <div class="description-container">
                        <header class="heading-container">
                          <time datetime="2018-02-15">Pick Life, February 15th, 2018</time>
                          <h3 class="title">
                            Morbi hendrerit tristique lobortis.
                          </h3>
                        </header>
                        <p class="description">
                          Donec ut urna eu diam tincidunt dapibus. Integer sit amet justo egestas, porta risus a, malesuada ante. Cras mattis justo id erat vulputate, in tempus diam dictum.
                        </p>
                      </div>
                      <div class="know-more"></div>
                    </div>
                  </article>
                </td>
              </tr>
              <tr>
                <td class="content-box curabitur">
                  <article>
                    <div class="info-box">
                      <div class="description-container">
                        <header class="heading-container">
                          <time datetime="2018-01-28">Pick Life, January 28th, 2018</time>
                          <h3 class="title">
                            Curabitur et sem vulputate, accumsan nunc eget, dictum massa.
                          </h3>
                        </header>
                        <p class="description">
                          Donec ut urna eu diam tincidunt dapibus. Integer sit amet justo egestas, porta risus a, malesuada ante. Cras mattis justo id erat vulputate, in tempus diam dictum.
                        </p>
                      </div>
                      <div class="know-more"></div>
                    </div>
                  </article>
                </td>
                <td class="content-box nulla">
                  <article>
                    <div class="info-box">
                      <div class="description-container">
                        <header class="heading-container">
                          <time datetime="2018-01-03">Pick Life, January 3th, 2018</time>
                          <h3 class="title">
                            Nulla dui ante, pretium sit amet auctor rhoncus, posuere eu felis.
                          </h3>
                        </header>
                        <p class="description">
                          Donec ut urna eu diam tincidunt dapibus. Integer sit amet justo egestas, porta risus a, malesuada ante. Cras mattis justo id erat vulputate, in tempus diam dictum.
                        </p>
                      </div>
                      <div class="know-more"></div>
                    </div>
                  </article>
                </td>
                <td class="content-box cras">
                  <article>
                    <div class="info-box">
                      <div class="description-container">
                        <header class="heading-container">
                          <time datetime="2017-12-20">Pick Life, December 20th, 2018</time>
                          <h3 class="title">
                            Cras et lorem eget enim pharetra facilisis eu ut urna.
                          </h3>
                        </header>
                        <p class="description">
                          Donec ut urna eu diam tincidunt dapibus. Integer sit amet justo egestas, porta risus a, malesuada ante. Cras mattis justo id erat vulputate, in tempus diam dictum.
                        </p>
                      </div>
                      <div class="know-more"></div>
                    </div>
                  </article>
                </td>
              </tr>
            </tbody>
          </table>
        </div>
      </section>
      <?php get_quote_banner() ?>
    </main>
    <?php get_top_footer() ?>
  </body>
</html>
