"use strict";

/* Propriedades e Métodos Gerais */

import "../global/libraries/theWheel/theWheel-min.js"; // theWheel

/* Identificadores */

import "../global/components/top-header/_dropdown-menu.js"; // Menu Deslizante do Cabeçalho

import "./_testimonials.js"; // Seção de Depoimentos

import "../global/components/_quote-banner.js"; // Banner com Formulário de Estimativa de Preço
