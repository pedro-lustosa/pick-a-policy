/* Construtores */

import { Slideshow } from "../global/components/constructors/_slideshow.js";

/* Identificadores */

const testimonials = document.getElementById( "testimonials" );
testimonials:
  { var slideshows = testimonials.getElementsByClassName( "slide-block" ),
        arrows = Array.from( testimonials.getElementsByClassName( "arrow" ) ),
        slideshowContent = testimonials.querySelector( ".slideshow-content" );
    slideshowContent:
      { var imagesList = Array.from( slideshowContent.getElementsByClassName( "slideshow-image" ) ),
            textsList = Array.from( slideshowContent.getElementsByClassName( "slideshow-text" ) ) }
    Slideshow:
      { for( let i = 0; i < slideshows.length; i++ ) eval( `var slideshow${ i + 1 } = new Slideshow( slideshows[ ${ i } ], imagesList, textsList, arrows )` ) } }
