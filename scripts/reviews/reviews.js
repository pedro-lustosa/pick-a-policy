"use strict";

/* Propriedades e Métodos Gerais */

import "../global/libraries/theWheel/theWheel-min.js"; // theWheel

/* Construtores */

/* Slideshow */
  // slider: The slideshow itself
  // imagesList: Array of all slideshow images, inside a hidden container
  // textsList: Array of all slideshow texts, inside a hidden container
  // displayedImage: The image being currently displayed by the slideshow
  // displayedText: The text being currently displayed by the slideshow
  // arrows: Set with the arrows for go to next or previous slide
  // previousArrow: Arrow that switches to the previous slide
  // nextArrow: Arrow that switches to the next slide

  export const Slideshow = function( slider, imagesList, textsList, arrows )
    { this.slider = slider; this.imagesList = imagesList; this.textsList = textsList; this.arrows = arrows;
      if( imagesList.length != textsList.length ) throw new Error( `The amount of images for the slideshow ( ${ imagesList.length } ) do not match with the amount of texts( ${ textsList.length } )` );
      var displayedImage = imagesList.find( image => image.querySelector( "img" ).src == slider.querySelector( ".slideshow-image img" ).src ),
          displayedText = textsList.find( text => text.querySelector( ".title" ).textContent == slider.querySelector( ".slideshow-text .title" ).textContent ),
          previousArrow = arrows.find( arrow => arrow.classList.contains( "previous" ) ),
          nextArrow = arrows.find( arrow => arrow.classList.contains( "next" ) ),
          slidersList = Slideshow.slidersList;
      if( !slidersList.length || !slidersList.some( sliderSet => sliderSet.includes( nextArrow ) ) ) slidersList.push( [ previousArrow, nextArrow, slider ] )
      else slidersList.find( sliderSet => sliderSet.includes( nextArrow ) ).push( slider );
      for( let arrow of arrows )
        { arrow.switchCount ? arrow.switchCount++ : arrow.switchCount = 1;
          arrow.addEventListener( "click", () =>
            { let slidersList = Slideshow.slidersList.find( sliderSet => sliderSet.includes( arrow ) ),
                  switchCount = arrow.switchCount - ( slidersList.filter( slider => window.getComputedStyle( slider ).display == "none" ).length );
              switchSlide( imagesList.oCycle( imagesList.indexOf( displayedImage ) + ( arrow == nextArrow ? switchCount : switchCount * -1 ) ),
                           textsList.oCycle( textsList.indexOf( displayedText ) + ( arrow == nextArrow ? switchCount : switchCount * -1 ) ) ) } ) };
      var switchSlide = ( newImage, newText ) =>
        { newImage.oTriggerMediaLoading();
          let displayedImage = slider.querySelector( ".slideshow-image" ), displayedText = slider.querySelector( ".slideshow-text" ),
              swapSets = [ [ displayedImage, newImage ], [ displayedText, newText ] ];
          for( let set of swapSets )  { set[ 0 ].replaceWith( set[ 1 ].cloneNode( true ) ); set[ 0 ].remove() };
          updateDisplayedContent( newImage, newText ) }
      var updateDisplayedContent = ( newImage, newText ) => { displayedImage = newImage; displayedText = newText } }

  /* Propriedades */
  Slideshow.slidersList = [];

/* Identificadores */

// Menu Deslizante do Cabeçalho
( function()
    { /* Identificadores */

    const topHeader = document.getElementById( "top-header" ); // Cabeçalho Superior da Página
    topHeader:
      { var dropdownMenu = topHeader.dropdownMenu = topHeader.querySelector( ".dropdown-menu" ) || topHeader.querySelector( ".nav-container" ); /* Menu Deslizante do Layout Responsivo */
        dropdownMenu:
          { var pagesList = dropdownMenu.list = dropdownMenu.querySelector( ".pages-list" ) } }

    /* Ouvintes de Evento */

    toggleDropdownMenu: // Expandir e Retrair Menu Responsivo do Cabeçalho Superior
      { dropdownMenu.addEventListener( "click", () =>
          { if( window.innerWidth >= 500 ) return;
            pagesList.offsetHeight ? pagesList.style.height = "0px" : pagesList.oEncompassChildren() } )
        dropdownMenu.addEventListener( "blur", () =>
          { if( window.innerWidth >= 500 ) return;
            pagesList.style.height = "0px" } ) } } )();

// Seção de Depoimentos
( function()
    { /* Identificadores */

    const testimonials = document.getElementById( "testimonials" );
    testimonials:
      { var slideshows = testimonials.getElementsByClassName( "slide-block" ),
            arrows = Array.from( testimonials.getElementsByClassName( "arrow" ) ),
            slideshowContent = testimonials.querySelector( ".slideshow-content" );
        slideshowContent:
          { var imagesList = Array.from( slideshowContent.getElementsByClassName( "slideshow-image" ) ),
                textsList = Array.from( slideshowContent.getElementsByClassName( "slideshow-text" ) ) }
        Slideshow:
          { for( let i = 0; i < slideshows.length; i++ ) eval( `var slideshow${ i + 1 } = new Slideshow( slideshows[ ${ i } ], imagesList, textsList, arrows )` ) } } } )();

// Banner com Formulário de Estimativa de Preço

( function()
    { /* Identificadores */

      const quoteBanner = document.getElementById( "quote-banner" ); // Banner com Formulário de Estimativa de Preço
      quoteBanner:
        { var heading = quoteBanner.heading = quoteBanner.querySelector( ".title" ); /* Título do Banner */
          heading:
            { var mainLine = heading.mainLine = heading.querySelector( ".main-line" ), /* Linha Principal do Título */
                  sideLine = heading.sideLine = heading.querySelector( ".side-line" ) /* Linha Periférica do Título */ } }

      /* Ouvintes de Evento */

      changeQuoteBannerText: // Alterar Texto do Banner com Formulário de Estimativa de Preço
        { for( let _event of [ "load", "resize" ] ) window.addEventListener( _event, () => mainLine.oChangeBySize( { innerWidth: 639 },
            mainLine.oChangeText.bind( mainLine, mainLine.dataset.text ), mainLine.oChangeText.bind( mainLine, mainLine.formerText || mainLine.textContent ) ) ) } } )();
