/* Identificadores */

const quoteBanner = document.getElementById( "quote-banner" ); // Banner com Formulário de Estimativa de Preço
quoteBanner:
  { var heading = quoteBanner.heading = quoteBanner.querySelector( ".title" ); /* Título do Banner */
    heading:
      { var mainLine = heading.mainLine = heading.querySelector( ".main-line" ), /* Linha Principal do Título */
            sideLine = heading.sideLine = heading.querySelector( ".side-line" ) /* Linha Periférica do Título */ } }

/* Ouvintes de Evento */

changeQuoteBannerText: // Alterar Texto do Banner com Formulário de Estimativa de Preço
  { for( let _event of [ "load", "resize" ] ) window.addEventListener( _event, () => mainLine.oChangeBySize( { innerWidth: 639 },
      mainLine.oChangeText.bind( mainLine, mainLine.dataset.text ), mainLine.oChangeText.bind( mainLine, mainLine.formerText || mainLine.textContent ) ) ) }
