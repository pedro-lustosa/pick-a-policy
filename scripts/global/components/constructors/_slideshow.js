/* Slideshow */

// slider: The slideshow itself
// imagesList: Array of all slideshow images, inside a hidden container
// textsList: Array of all slideshow texts, inside a hidden container
// displayedImage: The image being currently displayed by the slideshow
// displayedText: The text being currently displayed by the slideshow
// arrows: Set with the arrows for go to next or previous slide
// previousArrow: Arrow that switches to the previous slide
// nextArrow: Arrow that switches to the next slide

export const Slideshow = function( slider, imagesList, textsList, arrows )
  { this.slider = slider; this.imagesList = imagesList; this.textsList = textsList; this.arrows = arrows;
    if( imagesList.length != textsList.length ) throw new Error( `The amount of images for the slideshow ( ${ imagesList.length } ) do not match with the amount of texts( ${ textsList.length } )` );
    var displayedImage = imagesList.find( image => image.querySelector( "img" ).src == slider.querySelector( ".slideshow-image img" ).src ),
        displayedText = textsList.find( text => text.querySelector( ".title" ).textContent == slider.querySelector( ".slideshow-text .title" ).textContent ),
        previousArrow = arrows.find( arrow => arrow.classList.contains( "previous" ) ),
        nextArrow = arrows.find( arrow => arrow.classList.contains( "next" ) ),
        slidersList = Slideshow.slidersList;
    if( !slidersList.length || !slidersList.some( sliderSet => sliderSet.includes( nextArrow ) ) ) slidersList.push( [ previousArrow, nextArrow, slider ] )
    else slidersList.find( sliderSet => sliderSet.includes( nextArrow ) ).push( slider );
    for( let arrow of arrows )
      { arrow.switchCount ? arrow.switchCount++ : arrow.switchCount = 1;
        arrow.addEventListener( "click", () =>
          { let slidersList = Slideshow.slidersList.find( sliderSet => sliderSet.includes( arrow ) ),
                switchCount = arrow.switchCount - ( slidersList.filter( slider => window.getComputedStyle( slider ).display == "none" ).length );
            switchSlide( imagesList.oCycle( imagesList.indexOf( displayedImage ) + ( arrow == nextArrow ? switchCount : switchCount * -1 ) ),
                         textsList.oCycle( textsList.indexOf( displayedText ) + ( arrow == nextArrow ? switchCount : switchCount * -1 ) ) ) } ) };
    var switchSlide = ( newImage, newText ) =>
      { newImage.oTriggerMediaLoading();
        let displayedImage = slider.querySelector( ".slideshow-image" ), displayedText = slider.querySelector( ".slideshow-text" ),
            swapSets = [ [ displayedImage, newImage ], [ displayedText, newText ] ];
        for( let set of swapSets )  { set[ 0 ].replaceWith( set[ 1 ].cloneNode( true ) ); set[ 0 ].remove() };
        updateDisplayedContent( newImage, newText ) }
    var updateDisplayedContent = ( newImage, newText ) => { displayedImage = newImage; displayedText = newText } }

/* Propriedades */
Slideshow.slidersList = [];
