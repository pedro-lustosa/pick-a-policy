/* Sliding Box */

export const SlidingBox = function( container, headingContent, slidedContent, button )
  { this.container = container; this.headingContent = headingContent; this.slidedContent = slidedContent; this.button = button;
    blockRepeatedClicks: // Impedir o efeito de mais de um clique até que o deslizamento atual tenha se concluído
      { button.addEventListener( "click", () => button.oDisableEventType( "click" ) );
        slidedContent.addEventListener( "transitionend", event => button.oDisableEventType( "click", event.propertyName != "height" ) ) }
    showContentBody: // Mostrar seção de resposta de dado cabeçalho de pergunta
      { button.addEventListener( "click", () =>
          { container.classList.toggle( "active" ) ? slidedContent.oEncompassChildren( false ) : slidedContent.style.height = "" } ) } }
