/* Fading Content */

export const FadingContent = function( container, contentList, arrowsSet )
  { this.container = container; contentList = this.contentList = Array.from( contentList ); arrowsSet = this.arrowsSet = Array.from( arrowsSet );
    let displayedContent = contentList.displayedContent = contentList.filter( content => content.classList.contains( "displayed" ) ),
        previousArrow = arrowsSet.previousArrow = arrowsSet.find( arrow => arrow.name == "previous" ),
        nextArrow = arrowsSet.nextArrow = arrowsSet.find( arrow => arrow.name == "next" );
    setContentsPlacement.apply( null, displayedContent ); adjustHeight();
    nextArrow.addEventListener( "click", () =>
      { nextArrow.oDisableEventType( "click", true );
        toNext(); move( "top" ) } );
    previousArrow.addEventListener( "click", () =>
      { previousArrow.oDisableEventType( "click", true );
        toPrevious(); move( "bottom" ) } );
    setContentEvents.apply( null, contentList );
    function toNext()
      { let oldContent = displayedContent.shift(), newContent = container.appendChild( oldContent.cloneNode( true ) ),
            replacementContent = displayedContent.oCycle( -1 ).nextElementSibling || container.firstElementChild;
        newContent.classList.remove( "displayed" ); newContent.style = ""; setContentEvents( newContent );
        displayedContent.push( replacementContent );
        oldContent.style.animationName = "to-top";
        oldContent.style.transition = "none"; oldContent.style.transform = "";
        oldContent.oControlAnimation();
        replacementContent.classList.add( "displayed" ); setContentsPlacement( replacementContent ) }
    function toPrevious()
      { let oldContent = displayedContent.pop(), newContent = container.insertBefore( oldContent.cloneNode( true ), container.firstElementChild ),
            replacementContent = displayedContent[0].previousElementSibling || container.lastElementChild;
        newContent.classList.remove( "displayed" ); newContent.style = ""; setContentEvents( newContent );
        displayedContent.unshift( replacementContent );
        oldContent.style.animationName = "to-bottom"; oldContent.oControlAnimation();
        replacementContent.classList.add( "displayed" ); setContentsPlacement( replacementContent ) }
    function move( direction )
      { let operator, targetElement;
        if( direction == "top" )
          { operator = "-"; targetElement = displayedContent[0].previousElementSibling }
        if( direction == "bottom" )
          { operator = "+"; targetElement = displayedContent.oCycle( -1 ).nextElementSibling }
        for( let content of displayedContent )
          { let originalValue = Number( content.style.transform.replace( /[^1-9.+-]/g, "" ) ) * -1 || 0;
            content.style.transform = `translateY( ${ operator }${ originalValue + targetElement.offsetHeight + window.getComputedStyle( targetElement ).marginBottom.oFilterNumber() }px )` } }
    function setContentEvents()
      { for( let content of arguments ) content.addEventListener( "animationend", event =>
          { nextArrow.oDisableEventType( "click", false ); event.target.remove() } ) }
    function setContentsPlacement()
      { for( let content of arguments )
          { let previousContent = content.previousElementSibling, previousContentDisplacement = 0;
            if( previousContent ) previousContentDisplacement = Number( previousContent.style.transform.replace( /[^1-9.+-]/g, "" ) ) || 0;
            content.style.top = ( previousContent ?
              previousContent.offsetTop + previousContent.offsetHeight + window.getComputedStyle( previousContent ).marginBottom.oFilterNumber() + previousContentDisplacement
              : 0 ) + "px" } }
    function adjustHeight()
      { container.verticalSize = 0;
        for( let content of displayedContent ) container.verticalSize += content.offsetHeight + window.getComputedStyle( content ).marginBottom.oFilterNumber();
        container.style.height = container.verticalSize + "px" } }
