/* Identificadores */

const trackBanner = document.getElementById( "track-banner" ); // Banner de Histórico
trackBanner:
  { var textContainer = trackBanner.textContainer = trackBanner.querySelector( ".text-container" ); /* Recipiente de Texto do Banner */
    textContainer:
      { var textSet = textContainer.textSet = textContainer.querySelector( ".text-set" ) /* Conjunto de Textos do Banner */ }
    var displayText = trackBanner.displayText = trackBanner.querySelector( ".side-image" ) /* Texto Decorativo a Originalmente Aparecer ao Lado do Recipiente de Texto */ }

/* Ouvintes de Evento */

changeTrackBannerArranging: // Alterar Posicionamento da Imagem Lateral do Banner de Histórico para Dentro do Conteúdo Textual
  { for( let _event of [ "load", "resize" ] ) window.addEventListener( _event, () => displayText.oChangePlacementBySize( { innerWidth: 999 }, textContainer, textSet ) ) }
