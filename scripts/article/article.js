"use strict";

/* Propriedades e Métodos Gerais */

import "../global/libraries/theWheel/theWheel-min.js"; // theWheel

/* Identificadores */

// Menu Deslizante do Cabeçalho
( function()
    { /* Identificadores */

    const topHeader = document.getElementById( "top-header" ); // Cabeçalho Superior da Página
    topHeader:
      { var dropdownMenu = topHeader.dropdownMenu = topHeader.querySelector( ".dropdown-menu" ) || topHeader.querySelector( ".nav-container" ); /* Menu Deslizante do Layout Responsivo */
        dropdownMenu:
          { var pagesList = dropdownMenu.list = dropdownMenu.querySelector( ".pages-list" ) } }

    /* Ouvintes de Evento */

    toggleDropdownMenu: // Expandir e Retrair Menu Responsivo do Cabeçalho Superior
      { dropdownMenu.addEventListener( "click", () =>
          { if( window.innerWidth >= 500 ) return;
            pagesList.offsetHeight ? pagesList.style.height = "0px" : pagesList.oEncompassChildren() } )
        dropdownMenu.addEventListener( "blur", () =>
          { if( window.innerWidth >= 500 ) return;
            pagesList.style.height = "0px" } ) } } )();

// Banner com Formulário de Estimativa de Preço

( function()
    { /* Identificadores */

      const quoteBanner = document.getElementById( "quote-banner" ); // Banner com Formulário de Estimativa de Preço
      quoteBanner:
        { var heading = quoteBanner.heading = quoteBanner.querySelector( ".title" ); /* Título do Banner */
          heading:
            { var mainLine = heading.mainLine = heading.querySelector( ".main-line" ), /* Linha Principal do Título */
                  sideLine = heading.sideLine = heading.querySelector( ".side-line" ) /* Linha Periférica do Título */ } }

      /* Ouvintes de Evento */

      changeQuoteBannerText: // Alterar Texto do Banner com Formulário de Estimativa de Preço
        { for( let _event of [ "load", "resize" ] ) window.addEventListener( _event, () => mainLine.oChangeBySize( { innerWidth: 639 },
            mainLine.oChangeText.bind( mainLine, mainLine.dataset.text ), mainLine.oChangeText.bind( mainLine, mainLine.formerText || mainLine.textContent ) ) ) } } )();
