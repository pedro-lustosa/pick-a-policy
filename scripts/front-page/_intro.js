/* Identificadores */

const intro = document.getElementById( "intro" ); // Seção de Abertura da Página
intro:
  { var footerBlock = intro.footerBlock = intro.querySelector( ".footer-block" ); /* Subseção que Exibe Empresas Relacionadas a Pick a Policy */
    footerBlock:
      { var companiesList = footerBlock.companiesList = footerBlock.querySelector( ".companies-list" ), /* Lista de Empresas Relacionadas a Pick a Policy */
            displayerButton = footerBlock.displayerButton = footerBlock.querySelector( ".displayer-button" ); /* Botão para Expandir Lista de Empresas */
        displayerButton:
          { var buttonBody = displayerButton.body = displayerButton.querySelector( ".button-body" ) /* Parte Central do Botão, Clicável */ } } }

/* Ouvintes de Evento */

expandCompaniesList: // Expandir Lista de Empresas da Seção de Introdução
  { for( let _event of [ "load", "resize" ] )
      { window.addEventListener( _event, () => companiesList.oChangeBySize( { innerWidth: 1099 },
          companiesList.oMatchSize.bind( companiesList, companiesList.firstElementChild, { height: "offsetHeight" }, 2 ), companiesList.oFlatSize.bind( companiesList, companiesList.firstElementChild, false ) ) ) };
    buttonBody.addEventListener( "click", () =>
      { if( window.innerWidth >= 1100 )
          { buttonBody.classList.toggle( "x" ) ? companiesList.oEncompassChildren() : companiesList.oFlatSize( companiesList.firstElementChild, false ) }
        else
          { let childrenSet = Array.from( companiesList.children ).slice( 0, 2 );
            for( let child of childrenSet ) companiesList.append( child ) } } ) }

revalueLists: // Agrupar em <div>'s Itens da Lista de Empresas por Fileira
  { window.addEventListener( "resize", () => revalueGroupsNumber() ) }

changeButtonImage: // Altera a Imagem do Botão de Exibição das Logomarcas
  { window.addEventListener( "resize", () => { buttonBody.oChangeClassesBySize( { innerWidth: 1099 }, { add: "arrow-circle", remove: "plus-circle" } ) } ) }

/* Funções Específicas */

const findGroupItensNumber = function() // Encontra por Tamanho de Tela a Quantidade de <li>'s da Lista de Empresas Agrupada por <div>'s
  { var viewportWidth = window.innerWidth, groupItensNumber;
    switch( true )
      { case viewportWidth >= 950: groupItensNumber = 5; break;
        case viewportWidth < 950 && viewportWidth >= 800: groupItensNumber = 4; break;
        case viewportWidth < 800 && viewportWidth >= 600: groupItensNumber = 3; break;
        case viewportWidth < 600 && viewportWidth >= 450: groupItensNumber = 2; break;
        default: groupItensNumber = 1 }
    return groupItensNumber }

const agroupLists = function( groupItensNumber ) // Agrupar em <div>'s Itens da Lista de Empresas por Fileira
  { groupItensNumber = groupItensNumber || findGroupItensNumber();
    let listItens = companiesList.oBringChildren( companiesList.querySelectorAll( "li" ), undefined, 1 ),
        listItensNumber = listItens.length, listItensValues = listItens.values();
    for( let i = listItensNumber % groupItensNumber == 0 ? Math.trunc( listItensNumber/groupItensNumber ) : Math.trunc( listItensNumber/groupItensNumber ) + 1; i; i-- )
      { let li = companiesList.appendChild( document.createElement( "LI" ) ), ul = li.appendChild( document.createElement( "UL" ) );
        li.classList.add( "list-container" );
        for( let i = groupItensNumber; i; i-- )
          { let item = listItensValues.next().value; if( !item ) break;
            ul.appendChild( item ) } } }

const revalueGroupsNumber = function() // Avaliar se o Número de <div>'s Agupadoras dos Itens da Lista de Empresas precisa ser Redefinido
  { var targetNumber = findGroupItensNumber(), actualNumber = companiesList.childElementCount;
    if( targetNumber == actualNumber ) return;
    while( companiesList.firstElementChild.classList.contains( "list-container" ) )
      { let actualContainer = companiesList.firstElementChild,
            containerItens = actualContainer.oBringChildren( actualContainer.querySelectorAll( "li" ), undefined, 2 );
        for( let item of containerItens ) companiesList.appendChild( item );
        actualContainer.remove() }
    return agroupLists( targetNumber ) }

/* Instruções Iniciais */

buttonBody.oChangeClassesBySize( { innerWidth: 1099 }, { add: "arrow-circle", remove: "plus-circle" } ); agroupLists();
