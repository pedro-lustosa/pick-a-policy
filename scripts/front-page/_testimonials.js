/* Construtores */

import { Slideshow } from "../global/components/constructors/_slideshow.js";

/* Identificadores */

const testimonials = document.getElementById( "testimonials" );
testimonials:
  { var slideshow = testimonials.slideshow = testimonials.querySelector( ".testimonial-content" ),
        arrowsSet = testimonials.arrows = testimonials.querySelector( ".arrows-set" ),
        arrows = Array.from( arrowsSet.querySelectorAll( "[ class*='arr' ]" ) ),
        slideshowContent = slideshow.content = testimonials.querySelector( ".slideshow-content" );
    slideshowContent:
      { var imagesList = Array.from( slideshowContent.getElementsByClassName( "slideshow-image" ) ),
            textsList = Array.from( slideshowContent.getElementsByClassName( "slideshow-text" ) ) }
    Slideshow:
      { var slideshow1 = new Slideshow( slideshow, imagesList, textsList, arrows ) } }

/* Ouvintes de Evento */

slideshowSetup: // Funcionalidades do Slideshow
  { changeArrowsPlacement: // Em Telas de Pouca Largura, Deslocar Setas para Baixo do Slide
      { for( let _event of [ "load", "resize" ] )
          { window.addEventListener( _event, () =>
              { let displayedText = slideshow.querySelector( ".slideshow-text" );
                arrowsSet.oChangePlacementBySize( { innerWidth: 1129 }, slideshow, displayedText ) } ) } } }
