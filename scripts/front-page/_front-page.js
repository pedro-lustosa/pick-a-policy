"use strict";

/* Livrarias */

import "../global/libraries/theWheel/theWheel.js"; // The Wheel

/* Componentes */

import "../global/components/top-header/_dropdown-menu.js"; // Menu Deslizante

import "./_intro.js"; // Banner de Logomarcas da Empresa

import "./_life-events.js"; // Seção de Ilustrações sobre Benefícios de Seguros

import "../global/components/_track-banner.js"; // Banner de Histórico

import "./_testimonials.js"; // Seção de Depoimentos

import "../global/components/_quote-banner.js"; // Banner com Formulário de Estimativa de Preço
