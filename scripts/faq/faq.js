"use strict";

/* Propriedades e Métodos Gerais */

import "../global/libraries/theWheel/theWheel-min.js"; // theWheel

/* Identificadores */

// Menu Deslizante do Cabeçalho
( function()
    { /* Identificadores */

      const topHeader = document.getElementById( "top-header" ); // Cabeçalho Superior da Página
      topHeader:
        { var dropdownMenu = topHeader.dropdownMenu = topHeader.querySelector( ".dropdown-menu" ) || topHeader.querySelector( ".nav-container" ); /* Menu Deslizante do Layout Responsivo */
          dropdownMenu:
            { var pagesList = dropdownMenu.list = dropdownMenu.querySelector( ".pages-list" ) } }

      /* Ouvintes de Evento */

      toggleDropdownMenu: // Expandir e Retrair Menu Responsivo do Cabeçalho Superior
        { dropdownMenu.addEventListener( "click", () =>
            { if( window.innerWidth >= 500 ) return;
              pagesList.offsetHeight ? pagesList.style.height = "0px" : pagesList.oEncompassChildren() } )
          dropdownMenu.addEventListener( "blur", () =>
            { if( window.innerWidth >= 500 ) return;
              pagesList.style.height = "0px" } ) } } )();

// Seção com Tabela de Questões
( function()
    { /* Identificadores */

      const questionsBox = document.getElementById( "questions-box" ); // Seção com Tabela de Questões
      questionsBox:
        { var box = questionsBox.box = questionsBox.querySelector( ".questions-container" );
          box:
            { var questions = box.questions = Array.from( box.getElementsByClassName( "question" ) );
              questions:
                { var questionBody = questions.questionBody = questions.oSelectForEach( ".question-body" );
                  questionBody:
                    { var buttons = questionBody.buttons = questionBody.oSelectForEach( ".circle" ) }
                  var answersBody = questions.answerBody = questions.oSelectForEach( ".answer-body" ) } } }

      /* Ouvintes de Evento */

      showAnswerBody: // Mostrar Seção de Resposta de dado Cabeçalho de Pergunta
        { for( let button of buttons )
            { button.addEventListener( "click", () =>
                { let question = button.oBringParents( questions, 1 ), answer = question.oBringChildren( answersBody, 1 );
                  question.classList.toggle( "active" ) ? answer.oEncompassChildren( false ) : answer.style.height = "" } ) } } } )();

// Banner com Formulário de Estimativa de Preço
( function()
    { /* Identificadores */

      const quoteBanner = document.getElementById( "quote-banner" ); // Banner com Formulário de Estimativa de Preço
      quoteBanner:
        { var heading = quoteBanner.heading = quoteBanner.querySelector( ".title" ); /* Título do Banner */
          heading:
            { var mainLine = heading.mainLine = heading.querySelector( ".main-line" ), /* Linha Principal do Título */
                  sideLine = heading.sideLine = heading.querySelector( ".side-line" ) /* Linha Periférica do Título */ } }

      /* Ouvintes de Evento */

      changeQuoteBannerText: // Alterar Texto do Banner com Formulário de Estimativa de Preço
        { for( let _event of [ "load", "resize" ] ) window.addEventListener( _event, () => mainLine.oChangeBySize( { innerWidth: 639 },
            mainLine.oChangeText.bind( mainLine, mainLine.dataset.text ), mainLine.oChangeText.bind( mainLine, mainLine.formerText || mainLine.textContent ) ) ) } } )()
