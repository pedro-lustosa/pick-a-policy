/* Construtores */

import { SlidingBox } from "../global/components/constructors/_sliding-box.js";
import { FadingContent } from "../global/components/constructors/_fading-content.js";

/* Identificadores */

const questionsBox = document.getElementById( "questions-box" ); // Seção com Tabela de Questões
questionsBox:
  { var box = questionsBox.box = questionsBox.querySelector( ".questions-container" );
    box:
      { var questions = box.questions = Array.from( box.getElementsByClassName( "question" ) );
        questions:
          { var questionsHeaders = questions.headers = box.getElementsByClassName( "question-body" ),
                questionsAnswers = questions.answers = box.getElementsByClassName( "answer-body" ),
                questionsButtons = questions.buttons = box.getElementsByClassName( "circle" );
            for( let i = 0; i < questions.length; i++ ) new SlidingBox( questions[i], questionsHeaders[i], questionsAnswers[i], questionsButtons[i] ) } }
    var controlButtons = questionsBox.querySelector( ".content-container > .buttons-container" );
    new FadingContent( box, questions, controlButtons.children ) }

/* Ajustes */

const updateDisplayedQuestions = function()
  { return questions.displayed = questions.filter( question => question.classList.contains( "displayed" ) ) }

adjustPlacementWithAnswer:
  { for( let button of questionsButtons )
      { box:
          { button.addEventListener( "click", resizeBox ) }
        questions:
          { button.addEventListener( "click", replaceQuestions ) } }
    function resizeBox( event )
      { let verticalSize = 0, padding = [ 32 + 52 ],
            displayedQuestions = updateDisplayedQuestions();
        for( let question of displayedQuestions )
          { let answer = question.querySelector( ".answer-body" );
            if( question.contains( event.target ) && answer.offsetHeight )
              { verticalSize += ( question.offsetHeight - answer.offsetHeight ) + window.getComputedStyle( question ).marginBottom.oFilterNumber(); continue }
            else if( question.contains( event.target ) )
              { verticalSize += question.offsetHeight + window.getComputedStyle( question ).marginBottom.oFilterNumber() + answer.scrollHeight + padding[0]; continue }
            verticalSize += question.offsetHeight + window.getComputedStyle( question ).marginBottom.oFilterNumber() + ( answer.offsetHeight ? answer.scrollHeight + padding[0] : 0 ) }
        box.style.height = verticalSize + "px" }
    function replaceQuestions()
      {  } }
