"use strict";

/* Propriedades e Métodos Gerais */

import "../global/libraries/theWheel/theWheel.js"; // theWheel

/* Identificadores */

import "../global/components/top-header/_dropdown-menu.js"; // Menu Deslizante do Cabeçalho

import "./_questions-box.js"; // Seção com Tabela de Questões

import "../global/components/_quote-banner.js"; // Banner com Formulário de Estimativa de Preço
