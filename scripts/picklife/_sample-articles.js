/* Identificadores */

export const sampleArticles = document.getElementById( "sample-articles" );
sampleArticles:
  { var articles = sampleArticles.articles = sampleArticles.getElementsByTagName( "article" ),
        initialSet = articles.initialSet = sampleArticles.querySelector( ".initial-set" ),
        articlesCluster = articles.cluster = sampleArticles.querySelector( ".articles-cluster" ) }

export const media = { maxWidth: [ 1309 ] };

/* Funções */

const changeArticlesPlacement = function()
  { for( let article of arguments )
      { article.oChangePlacementBySize( { innerWidth: media.maxWidth[0] }, initialSet, articlesCluster );
        article.oChangeClassesBySize( { innerWidth: media.maxWidth[0] }, { add: "featured" } ) } }

/* Eventos */

changeToActive: // Altera aparência do artigo de amostra ao repousar sobre ele o ponteiro do mouse
  { for( let article of articles )
      { article.addEventListener( "mouseenter", () =>
          { if( article.classList.contains( "featured" ) ) return;
            article.classList.add( "active" ); article.style.backgroundImage = "linear-gradient( 0deg, hsla( 218, 45%, 47%, .9 ), hsla( 218, 45%, 47%, .9 ) ), " + window.getComputedStyle( article ).backgroundImage } );
        article.addEventListener( "mouseleave", () =>
          { if( article.classList.contains( "featured" ) ) return;
            article.classList.remove( "active" ); article.style.backgroundImage = "" } ) } }

arrangePlacement: // Altera posicionamento dos artigos em telas de pouca largura
  { changeArticlesPlacement( articles[2], articles[1] );
    window.addEventListener( "resize", () => changeArticlesPlacement( articles[2], articles[1] ) ) }
