"use strict";

/* Propriedades e Métodos Gerais */

import "../global/libraries/theWheel/theWheel-min.js"; // theWheel

/* Identificadores */

// Menu Deslizante do Cabeçalho
( function()
    { /* Identificadores */

    const topHeader = document.getElementById( "top-header" ); // Cabeçalho Superior da Página
    topHeader:
      { var dropdownMenu = topHeader.dropdownMenu = topHeader.querySelector( ".dropdown-menu" ) || topHeader.querySelector( ".nav-container" ); /* Menu Deslizante do Layout Responsivo */
        dropdownMenu:
          { var pagesList = dropdownMenu.list = dropdownMenu.querySelector( ".pages-list" ) } }

    /* Ouvintes de Evento */

    toggleDropdownMenu: // Expandir e Retrair Menu Responsivo do Cabeçalho Superior
      { dropdownMenu.addEventListener( "click", () =>
          { if( window.innerWidth >= 500 ) return;
            pagesList.offsetHeight ? pagesList.style.height = "0px" : pagesList.oEncompassChildren() } )
        dropdownMenu.addEventListener( "blur", () =>
          { if( window.innerWidth >= 500 ) return;
            pagesList.style.height = "0px" } ) } } )();

// Amostra de Artigos do Picklife
( function ()
    { /* Identificadores */

    const sampleArticles = document.getElementById( "sample-articles" );
    sampleArticles:
      { var articles = sampleArticles.articles = sampleArticles.getElementsByTagName( "article" ),
            initialSet = articles.initialSet = sampleArticles.querySelector( ".initial-set" ),
            articlesCluster = articles.cluster = sampleArticles.querySelector( ".articles-cluster" ) }

    const media = { maxWidth: [ 1309 ] };

    /* Funções */

    const changeArticlesPlacement = function()
      { for( let article of arguments )
          { article.oChangePlacementBySize( { innerWidth: media.maxWidth[0] }, initialSet, articlesCluster );
            article.oChangeClassesBySize( { innerWidth: media.maxWidth[0] }, { add: "featured" } ) } }

    /* Eventos */

    changeToActive: // Altera aparência do artigo de amostra ao repousar sobre ele o ponteiro do mouse
      { for( let article of articles )
          { article.addEventListener( "mouseenter", () =>
              { if( article.classList.contains( "featured" ) ) return;
                article.classList.add( "active" ); article.style.backgroundImage = "linear-gradient( 0deg, hsla( 218, 45%, 47%, .9 ), hsla( 218, 45%, 47%, .9 ) ), " + window.getComputedStyle( article ).backgroundImage } );
            article.addEventListener( "mouseleave", () =>
              { if( article.classList.contains( "featured" ) ) return;
                article.classList.remove( "active" ); article.style.backgroundImage = "" } ) } }

    arrangePlacement: // Altera posicionamento dos artigos em telas de pouca largura
      { changeArticlesPlacement( articles[2], articles[1] );
        window.addEventListener( "resize", () => changeArticlesPlacement( articles[2], articles[1] ) ) } } )();

// Banner com Formulário de Estimativa de Preço

( function()
    { /* Identificadores */

      const quoteBanner = document.getElementById( "quote-banner" ); // Banner com Formulário de Estimativa de Preço
      quoteBanner:
        { var heading = quoteBanner.heading = quoteBanner.querySelector( ".title" ); /* Título do Banner */
          heading:
            { var mainLine = heading.mainLine = heading.querySelector( ".main-line" ), /* Linha Principal do Título */
                  sideLine = heading.sideLine = heading.querySelector( ".side-line" ) /* Linha Periférica do Título */ } }

      /* Ouvintes de Evento */

      changeQuoteBannerText: // Alterar Texto do Banner com Formulário de Estimativa de Preço
        { for( let _event of [ "load", "resize" ] ) window.addEventListener( _event, () => mainLine.oChangeBySize( { innerWidth: 639 },
            mainLine.oChangeText.bind( mainLine, mainLine.dataset.text ), mainLine.oChangeText.bind( mainLine, mainLine.formerText || mainLine.textContent ) ) ) } } )();
