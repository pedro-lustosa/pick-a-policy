<footer id="top-footer">
  <section class="contact-section">
    <div class="content-container">
      <div class="contact-row">
        <?php get_logo_img() ?>
        <div class="contact-info">
          <addres class="company-address">
            <p class="description">
              <span class="line">12001 Research Parkway</span>
              <span class="line">Suite 236</span>
              <span class="line">Orlando Florida 32826</span>
            </p>
          </addres>
          <address class="company-tel">
            <span class="number">&lpar;800&rpar; 279&ndash;1455</span>
          </address>
        </div>
        <?php
          get_social_list( array( 'twitter' => '#', 'facebook' => '#', 'youtube' => '#', 'linkedin' => '#' ) );
        ?>
      </div>
      <div class="form-row">
        <form name="quote-form-3" action="" method="post">
          <header class="heading-container">
            <h1 class="title"><span class="removable">Ready to </span>Quote your Life Insurance<span class="removable">?</span></h1>
          </header>
          <fieldset class="zip-field">
            <label for="zip-3" hidden>Zip Code</label>
            <input id="zip-3" name="zip" type="text" placeholder="Enter your zip code here">
            <button type="submit" name="quote">Get a Quote Now</button>
          </fieldset>
        </form>
        <form name="newsletter-form-1" action="" method="post">
          <header class="heading-container">
            <h1 class="title">Subscribe your Newsletter</h1>
          </header>
          <fieldset class="newsletter-field">
            <label for="newsletter-1" hidden>Newsletter</label>
            <input id="newsletter-1" name="newsletter" type="email" placeholder="Enter your email">
            <button type="submit" name="newsletter">Subscribe Now</button>
          </fieldset>
        </form>
      </div>
    </div>
  </section>
  <nav class="links-section">
    <div class="content-container">
      <ul class="links-set magazine">
        <li><a href="#">Magazine</a></li>
        <li><a href="#">About</a></li>
        <li><a href="#">Contact</a></li>
      </ul>
      <dl class="links-set product-learn">
        <dt class="title">Product Learn Centers</dt>
        <dd class="content">
          <ul class="product-learn-list">
            <li><a href="">Life Insurance<span class="removable"> Learn Center</span></a></li>
            <li><a href="">Health Insurance<span class="removable"> Learn Center</span></a></li>
            <li><a href="">Disability Insurance<span class="removable"> Learn Center</span></a></li>
            <li><a href="">Pet Insurance<span class="removable"> Learn Center</span></a></li>
            <li><a href="">Renters Insurance<span class="removable"> Learn Center</span></a></li>
          </ul>
        </dd>
      </dl>
      <dl class="links-set picklife">
        <dt class="title">PickLife</dt>
        <dd class="content">
          <ul class="picklife-list">
            <li><a href="">Money</a></li>
            <li><a href="">Tech</a></li>
            <li><a href="">How-Tos</a></li>
            <li><a href="">Interviews</a></li>
            <li><a href="">News</a></li>
          </ul>
        </dd>
      </dl>
      <dl class="links-set resources">
        <dt class="title">Useful Resources</dt>
        <dd class="content">
          <ul class="resources-list">
            <li><a href="">Life Insurance Advice</a></li>
            <li><a href="">Health Insurance Marketplace</a></li>
            <li><a href="">Obamacare</a></li>
            <li><a href="">Life Insurance Calculator</a></li>
          </ul>
        </dd>
      </dl>
      <dl class="links-set reviews">
        <dt class="title">Reviews</dt>
        <dd class="content">
          <ul class="reviews-list">
            <li><a href="">Trust Pilot Reviews</a></li>
          </ul>
        </dd>
      </dl>
    </div>
  </nav>
  <section class="copyright-section">
    <div class="content-container">
      <nav class="nav-container">
        <picture class="logo">
          <img src="multimedia/images/x-small/xs-logo-1.png" alt="Norton logo">
        </picture>
        <ul class="pages-list">
          <li><a href="">Trust &amp; Security</a></li>
          <li><a href="">Privacy Policy</a></li>
          <li><a href="">Terms</a></li>
          <li><a href="">Licenses</a></li>
        </ul>
      </nav>
      <div class="text-block">
        <div class="text-set">
          <p class="text">
            Yes, we have to include some legalese down here. Read it larger on our legal page. Pick a Policy Inc. (“PickaPolicy”) is a licensed independent insurance broker. Policygenius does not underwrite any insurance policy described on this website. The information provided on this site has been developed by Policygenius for general informational and educational purposes. We do our best efforts to ensure that this information is up-to-date and accurate. Any insurance policy premium quotes or ranges displayed are non-binding. The final insurance policy premium for any policy is determined by the underwriting insurance company following application. Savings are estimated by comparing the highest and lowest price for a shopper in a given health class. For example: for a 30-year old non-smoker male in South Carolina with excellent health and a preferred plus health class, comparing quotes for a $500,000, 20-year term life policy, the price difference between the lowest and highest quotes is 60%. For that same shopper in New York, the price difference is 40%. Rates are subject to change and are valid as of 2/17/17.
          </p>
        </div>
        <div class="copyright">
          <span class="message">Copyright PickaPolicy &copy; 2018</span>
        </div>
      </div>
    </div>
  </section>
</footer>
