<section id="quote-banner">
  <div class="banner">
    <div class="content-container">
      <header class="heading-container">
        <h1 class="title">
          <span class="side-line">How About</span>
          <span data-text="Leave a Legacy" class="main-line">Leaving a Legacy?</span>
        </h1>
      </header>
      <form name="quote-form-2" action="" method="post" autocomplete="off">
        <main class="form-body">
          <fieldset class="zip-field">
            <label for="zip-2">Zip Code</label>
            <input id="zip-2" name="zip" type="text" placeholder="Enter your zip code here">
          </fieldset>
          <button type="submit" name="quote">Get a Quote Now</button>
        </main>
        <footer class="form-footer">
          <p class="message">Need to see more? <a href="#">Start here.</a></p>
        </footer>
      </form>
    </div>
  </div>
</section>
