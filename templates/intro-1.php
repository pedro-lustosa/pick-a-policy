<div id="intro">
  <div class="content-container">
    <header class="heading-container">
      <h1 class="title">
        <?php
          if( !$lines ): echo $title;
          else:
            foreach( $lines as $line_break ):
              if( $lines[0] == $line_break ):
                echo "<span class='line'>" . strstr( $title, $line_break, true ) . "</span>";
                $remaining_str = $title;
              endif;
              echo "<span class='line'>" .
                   ( strstr( $remaining_str, $line_break ) ) .
                   "</span>";
              $remaining_str = substr( strstr( $remaining_str, $line_break ), mb_strlen( $line_break ) );
            endforeach;
          endif;
        ?>
      </h1>
    </header>
  </div>
</div>
