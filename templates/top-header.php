<header id="top-header">
  <div class="content-container">
    <?php get_logo_img() ?>
    <div class="menu-container">
      <nav tabindex="0" class="nav-container">
        <ul class="pages-list">
          <?php
            foreach( $pages as $name => $href ):
              echo "<li><a href='$href?page=$name'" .
                   ( isset( $_GET[ 'page' ] ) && $_GET[ 'page' ] == $name ? " class='current'" : "" ) .
                   ">$name</a></li>";
            endforeach;
          ?>
        </ul>
        <?php
          get_social_list( array( 'twitter' => '#', 'facebook' => '#', 'youtube' => '#', 'linkedin' => '#' ) );
        ?>
      </nav>
      <div class="button-container">
        <button type="button" name="quote">Get a Quote Now</button>
      </div>
    </div>
  </div>
</header>
