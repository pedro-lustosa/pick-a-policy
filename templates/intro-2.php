<div id="intro">
  <div class="content-container">
    <header class="heading-container">
      <hgroup class="heading-set">
        <h1 class="title"><?= $title ?></h1>
        <h2 class="subtitle"><?= $subtitle ?></h2>
      </hgroup>
      <?php if( $side_image ): ?>
        <picture class="side-image">
          <img src="multimedia/images/x-small/<?= $side_image ?>" alt="">
        </picture>
      <?php endif ?>
    </header>
  </div>
</div>
