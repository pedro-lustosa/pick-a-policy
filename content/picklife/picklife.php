<?php require "../../functions.php"; ?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="author" content="This theme was created by the brazilian company DR Estúdio &ndash; https://danielrothier.com">
    <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1">

    <base href="../../" target="_self">
    <title>Picklife</title>

    <link rel="icon" href="multimedia/images/x-small/xs-favicon-1.jpg">

    <link rel="stylesheet" href="styles/picklife/picklife-min.css">
    <script defer src="scripts/picklife/picklife-min.js" type="module"></script>
  </head>
  <body>
    <?php get_top_header( array( 'Magazine' => '#', 'About' => 'content/about-us.php', 'Contact' => '#' ) ) ?>
    <main id="top-content">
      <?php get_intro_2( 'picklife', 'Your lifestyle magazine', "xs-ilustration-1.png" ) ?>
      <table id="picklife-opening-table" class="articles-block">
        <tbody class="articles-group">
          <tr>
            <td rowspan="2" colspan="2" class="content-box dolorum featured">
              <article>
                <div class="info-box">
                  <div class="description-container">
                    <header class="heading-container">
                      <time datetime="2018-04-06">Pick Life, April 6th, 2018</time>
                      <h3 class="title">
                        Dolorum Aspernatur Natoque Conubia Occaecat
                      </h3>
                    </header>
                    <p class="description">
                      Accusamus metus viverra inceptos omnis tempus suscipit, sollicitudin, sint perferendis dolorem incididunt occaecati viverra excepturi. Curabitur et sem vulputate, accumsan nunc eget, dictum massa.
                    </p>
                  </div>
                  <div class="know-more"></div>
                </div>
              </article>
            </td>
            <td class="content-box etiam featured">
              <article>
                <div class="info-box">
                  <div class="description-container">
                    <header class="heading-container">
                      <time datetime="2018-03-03">Pick Life, March 3th, 2018</time>
                      <h3 class="title">
                        Etiam justo quam, ultricies non feugiat ne, eleifend id tellus.
                      </h3>
                    </header>
                    <p class="description">
                      Donec ut urna eu diam tincidunt dapibus. Integer sit amet justo egestas, porta risus a, malesuada ante. Cras mattis justo id erat vulputate, in tempus diam dictum.
                    </p>
                  </div>
                  <div class="know-more"></div>
                </div>
              </article>
            </td>
          </tr>
          <tr>
            <td class="content-box morbi">
              <article>
                <div class="info-box">
                  <div class="description-container">
                    <header class="heading-container">
                      <time datetime="2018-02-15">Pick Life, February 15th, 2018</time>
                      <h3 class="title">
                        Morbi hendrerit tristique lobortis.
                      </h3>
                    </header>
                    <p class="description">
                      Donec ut urna eu diam tincidunt dapibus. Integer sit amet justo egestas, porta risus a, malesuada ante. Cras mattis justo id erat vulputate, in tempus diam dictum.
                    </p>
                  </div>
                  <div class="know-more"></div>
                </div>
              </article>
            </td>
          </tr>
        </tbody>
      </table>
      <form id="search-bar" method="post">
        <fieldset class="input-field">
          <picture class="side-image">
            <img src="multimedia/images/x-small/xs-search-1.png" alt="">
          </picture>
          <input name="picklife-search-bar" type="search" placeholder="Searching for...">
        </fieldset>
        <div class="filter-tags">
          <span class="title">Quick Filters:</span>
          <div class="output-set">
            <output class="tag">
              <span class="content-container">
                <span class="content">Sports</span>
                <span class="remove">
                  <span class="x">&times;</span>
                </span>
              </span>
            </output>
            <output class="tag">
              <span class="content-container">
                <span class="content">House</span>
                <span class="remove">
                  <span class="x">&times;</span>
                </span>
              </span>
            </output>
          </div>
        </div>
      </form>
      <section id="sample-articles">
        <div class="content-container">
          <div class="initial-set">
            <article class="article-1 featured">
              <div class="image-box">
                <picture class="article-image">
                  <img src="multimedia/images/medium/m-decorative-1.jpg" alt="">
                </picture>
              </div>
              <div class="info-box">
                <div class="description-container">
                  <header class="heading-container">
                    <time datetime="2018-04-06">Pick Life, April 6th, 2018</time>
                    <h3 class="title">Felis dapibus. Asperiores vitae, asperiores.</h3>
                  </header>
                  <p class="description">
                    Natus officiis quo, adipisci eaque ipsa, architecto pellentesque? Ullam repudiandae sodales hymenaeos! Esse potenti vel dolor autem nam ad minus eligendi tristique. Urna et. Fermentum similique iaculis doloribus, provident aptent molestias, convallis condimentum recusandae suscipit arcu conubia quos eius saepe...
                  </p>
                </div>
                <div class="know-more"></div>
              </div>
            </article>
            <div class="articles-cluster">
              <article class="article-2">
                <div class="image-box">
                  <picture class="article-image">
                    <img src="multimedia/images/medium/m-decorative-2.jpg" alt="">
                  </picture>
                </div>
                <div class="info-box">
                  <div class="description-container">
                    <header class="heading-container">
                      <time datetime="2018-04-06">Pick Life, April 6th, 2018</time>
                      <h3 class="title">Cupiditate magni minus? Sapien cursus aperiam.</h3>
                    </header>
                    <p class="description">
                      Natus officiis quo, adipisci eaque ipsa, architecto pellentesque? Ullam repudiandae sodales hymenaeos! Esse potenti vel dolor autem nam ad minus eligendi tristique. Urna et. Fermentum similique iaculis doloribus, provident aptent molestias, convallis condimentum recusandae suscipit arcu conubia quos eius saepe...
                    </p>
                  </div>
                  <div class="know-more"></div>
                </div>
              </article>
              <article class="article-3">
                <div class="image-box">
                  <picture class="article-image">
                    <img src="multimedia/images/medium/m-decorative-3.jpg" alt="">
                  </picture>
                </div>
                <div class="info-box">
                  <div class="description-container">
                    <header class="heading-container">
                      <time datetime="2018-04-06">Pick Life, April 6th, 2018</time>
                      <h3 class="title">Cupiditate magni minus? Sapien cursus aperiam.</h3>
                    </header>
                    <p class="description">
                      Natus officiis quo, adipisci eaque ipsa, architecto pellentesque? Ullam repudiandae sodales hymenaeos! Esse potenti vel dolor autem nam ad minus eligendi tristique. Urna et. Fermentum similique iaculis doloribus, provident aptent molestias, convallis condimentum recusandae suscipit arcu conubia quos eius saepe...
                    </p>
                  </div>
                  <div class="know-more"></div>
                </div>
              </article>
              <article class="article-4">
                <div class="image-box">
                  <picture class="article-image">
                    <img src="multimedia/images/medium/m-decorative-4.jpg" alt="">
                  </picture>
                </div>
                <div class="info-box">
                  <div class="description-container">
                    <header class="heading-container">
                      <time datetime="2018-04-06">Pick Life, April 6th, 2018</time>
                      <h3 class="title">Cupiditate magni minus? Sapien cursus aperiam.</h3>
                    </header>
                    <p class="description">
                      Natus officiis quo, adipisci eaque ipsa, architecto pellentesque? Ullam repudiandae sodales hymenaeos! Esse potenti vel dolor autem nam ad minus eligendi tristique. Urna et. Fermentum similique iaculis doloribus, provident aptent molestias, convallis condimentum recusandae suscipit arcu conubia quos eius saepe...
                    </p>
                  </div>
                  <div class="know-more"></div>
                </div>
              </article>
              <article class="article-5">
                <div class="image-box">
                  <picture class="article-image">
                    <img src="multimedia/images/medium/m-decorative-5.jpg" alt="">
                  </picture>
                </div>
                <div class="info-box">
                  <div class="description-container">
                    <header class="heading-container">
                      <time datetime="2018-04-06">Pick Life, April 6th, 2018</time>
                      <h3 class="title">Cupiditate magni minus? Sapien cursus aperiam.</h3>
                    </header>
                    <p class="description">
                      Natus officiis quo, adipisci eaque ipsa, architecto pellentesque? Ullam repudiandae sodales hymenaeos! Esse potenti vel dolor autem nam ad minus eligendi tristique. Urna et. Fermentum similique iaculis doloribus, provident aptent molestias, convallis condimentum recusandae suscipit arcu conubia quos eius saepe...
                    </p>
                  </div>
                  <div class="know-more"></div>
                </div>
              </article>
            </div>
          </div>
          <article class="article-6">
            <div class="image-box">
              <picture class="article-image">
                <img src="multimedia/images/medium/m-decorative-6.jpg" alt="">
              </picture>
            </div>
            <div class="info-box">
              <div class="description-container">
                <header class="heading-container">
                  <time datetime="2018-04-06">Pick Life, April 6th, 2018</time>
                  <h3 class="title">Cupiditate magni minus? Sapien cursus aperiam.</h3>
                </header>
                <p class="description">
                  Natus officiis quo, adipisci eaque ipsa, architecto pellentesque? Ullam repudiandae sodales hymenaeos! Esse potenti vel dolor autem nam ad minus eligendi tristique. Urna et. Fermentum similique iaculis doloribus, provident aptent molestias, convallis condimentum recusandae suscipit arcu conubia quos eius saepe...
                </p>
              </div>
              <div class="know-more"></div>
            </div>
          </article>
          <article class="article-7">
            <div class="image-box">
              <picture class="article-image">
                <img src="multimedia/images/medium/m-decorative-7.jpg" alt="">
              </picture>
            </div>
            <div class="info-box">
              <div class="description-container">
                <header class="heading-container">
                  <time datetime="2018-04-06">Pick Life, April 6th, 2018</time>
                  <h3 class="title">Cupiditate magni minus? Sapien cursus aperiam.</h3>
                </header>
                <p class="description">
                  Natus officiis quo, adipisci eaque ipsa, architecto pellentesque? Ullam repudiandae sodales hymenaeos! Esse potenti vel dolor autem nam ad minus eligendi tristique. Urna et. Fermentum similique iaculis doloribus, provident aptent molestias, convallis condimentum recusandae suscipit arcu conubia quos eius saepe...
                </p>
              </div>
              <div class="know-more"></div>
            </div>
          </article>
          <article class="article-8">
            <div class="image-box">
              <picture class="article-image">
                <img src="multimedia/images/medium/m-decorative-8.jpg" alt="">
              </picture>
            </div>
            <div class="info-box">
              <div class="description-container">
                <header class="heading-container">
                  <time datetime="2018-04-06">Pick Life, April 6th, 2018</time>
                  <h3 class="title">Cupiditate magni minus? Sapien cursus aperiam.</h3>
                </header>
                <p class="description">
                  Natus officiis quo, adipisci eaque ipsa, architecto pellentesque? Ullam repudiandae sodales hymenaeos! Esse potenti vel dolor autem nam ad minus eligendi tristique. Urna et. Fermentum similique iaculis doloribus, provident aptent molestias, convallis condimentum recusandae suscipit arcu conubia quos eius saepe...
                </p>
              </div>
              <div class="know-more"></div>
            </div>
          </article>
        </div>
      </section>
      <section id="bottom-articles">
        <div class="content-container">
          <article class="article-1">
            <div class="image-box">
              <picture class="article-image">
                <img src="multimedia/images/medium/m-people-5.jpg" alt="">
              </picture>
            </div>
            <div class="info-box">
              <div class="description-container">
                <header class="heading-container">
                  <time datetime="2018-04-06">Pick Life, April 6th, 2018</time>
                  <h3 class="title">Temporibus accusantium quod iure, sapien autem.</h3>
                </header>
                <p class="description">
                  Natus officiis quo, adipisci eaque ipsa, architecto pellentesque? Ullam repudiandae sodales hymenaeos! Esse potenti vel dolor autem nam ad minus eligendi tristique.
                  Urna et. Fermentum similique iaculis doloribus, provident aptent molestias, convallis...
                </p>
              </div>
              <div class="know-more"></div>
            </div>
          </article>
          <article class="article-2">
            <div class="image-box">
              <picture class="article-image">
                <img src="multimedia/images/medium/m-people-6.jpg" alt="">
              </picture>
            </div>
            <div class="info-box">
              <div class="description-container">
                <header class="heading-container">
                  <time datetime="2018-04-06">Pick Life, April 6th, 2018</time>
                  <h3 class="title">Temporibus accusantium quod iure, sapien autem.</h3>
                </header>
                <p class="description">
                  Natus officiis quo, adipisci eaque ipsa, architecto pellentesque? Ullam repudiandae sodales hymenaeos! Esse potenti vel dolor autem nam ad minus eligendi tristique.
                  Urna et. Fermentum similique iaculis doloribus, provident aptent molestias, convallis...
                </p>
              </div>
              <div class="know-more"></div>
            </div>
          </article>
        </div>
      </section>
      <?php get_quote_banner() ?>
    </main>
    <?php get_top_footer() ?>
  </body>
</html>
