<?php require "../../functions.php"; ?>
<!DOCTYPE html>
<html lang="en-US">
  <head>
    <meta charset="utf-8">
    <meta name="author" content="This theme was created by the brazilian company DR Estúdio &ndash; https://danielrothier.com">
    <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1">

    <base href="../../" target="_self">
    <title>Contact Us</title>

    <link rel="icon" href="multimedia/images/x-small/xs-favicon-1.jpg">

    <link rel="stylesheet" href="styles/contact-us/contact-us-min.css">
    <script defer src="scripts/contact-us/contact-us-min.js" type="module"></script>
  </head>
  <body>
    <?php get_top_header( array( 'Magazine' => '#', 'About' => 'content/about-us.php', 'Contact' => '#' ) ) ?>
    <main id="top-content">
      <?php get_intro_1( 'Contact Us' ) ?>
      <section id="contact-section">
        <div class="content-container">
          <form name="main-contact-form" method="post" autocomplete="on">
            <header class="heading-container">
              <h2 class="title">
                <span class="line">We're experts in policy</span>
                <span class="line">Write a message to us!</span>
              </h2>
            </header>
            <div class="form-body">
              <input name="main-contact-name" type="text" placeholder="What's your name?" required>
              <input name="main-contact-email" type="email" placeholder="What's your e-mail?" required>
              <textarea name="main-contact-message" cols="80" placeholder="Your message" required></textarea>
            </div>
            <button name="main-contact-btn" type="submit">
              <span class="content">Send Message</span>
            </button>
          </form>
          <ul class="address-info-list">
            <li class="phone">
              <picture class="ilustration">
                <img src="multimedia/images/x-small/xs-ilustration-5.png" alt="">
              </picture>
              <dl>
                <dt>Phone</dt>
                <dd>(123) 456-7890</dd>
              </dl>
            </li>
            <li class="fax">
              <picture class="ilustration">
                <img src="multimedia/images/x-small/xs-ilustration-6.png" alt="">
              </picture>
              <dl>
                <dt>Fax</dt>
                <dd>(123) 456-7890</dd>
              </dl>
            </li>
            <li class="address">
              <picture class="ilustration">
                <img src="multimedia/images/x-small/xs-ilustration-7.png" alt="">
              </picture>
              <dl>
                <dt>Address</dt>
                <dd>38 Lorem Ipsum Street, City, Country</dd>
              </dl>
            </li>
          </ul>
        </div>
      </section>
      <?php get_quote_banner() ?>
    </main>
    <?php get_top_footer() ?>
  </body>
</html>
