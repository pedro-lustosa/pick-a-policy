<?php require "../../functions.php"; ?>
<!DOCTYPE html>
<html lang="en-US">
  <head>
    <meta charset="utf-8">
    <meta name="author" content="This theme was created by the brazilian company DR Estúdio &ndash; https://danielrothier.com">
    <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1">

    <base href="../../" target="_self">
    <title>Article</title>

    <link rel="icon" href="multimedia/images/x-small/xs-favicon-1.jpg">

    <link rel="stylesheet" href="styles/article/article-min.css">
    <script defer src="scripts/article/article-min.js" type="module"></script>
  </head>
  <body>
    <?php get_top_header( array( 'Magazine' => '#', 'About' => 'content/about-us.php', 'Contact' => '#' ) ) ?>
    <main id="top-content">
      <div id="intro"></div>
      <article id="article-title">
        <div class="content-container">
          <header class="heading-container">
            <h1 class="title">
              Pulvinar congue auctor fringilla feugiat eget sagittis
            </h1>
            <time datetime="2018-08-07">July 8, 2018</time>
            <ul class="social-list">
              <li><a class="twitter" href="#"></a></li>
              <li><a class="facebook" href="#"></a></li>
              <li><a class="share" href="#"></a></li>
              <li><a class="email" href="#"></a></li>
              <li><a class="link" href="#"></a></li>
            </ul>
          </header>
          <div class="text-container">
            <p class="text">
              Vero conubia nostrud, viverra pharetra conubia reprehenderit! Incididunt magna elementum eius minus. Harum. Cras autem eius semper varius! Diam proin? Rem convallis rerum atque? Quis consectetuer purus commodo, quaerat facilis. Soluta diam? Mollis ut, taciti deserunt. Odit pretium! Voluptatum sollicitudin, sapiente curae! Maxime vel! Ipsum erat pariatur aperiam nisl ultrices consectetur!
              Turpis diam tempore, adipiscing animi tincidunt nemo, aute consectetur lacus mauris commodi.
            </p>
            <p class="text">
              Iure illum irure arcu netus, velit? Ducimus fuga tenetur ea tempus adipisci! Porta. Laboris taciti, augue condimentum sociosqu nobis aliquid! Dolorum habitasse condimentum perspiciatis quas numquam ullam quo esse accusantium quam.
              Phasellus diam veritatis metus quaerat! Consequatur voluptates? Porta cupiditate? Quibusdam nostra. Impedit blanditiis sed, metus euismod pede, ea nibh quod tempus perspiciatis iste, quas impedit, cumque donec qui senectus, porro porro per.
            </p>
            <p class="text">
              Est mollitia accumsan dapibus. Blanditiis porro minima rutrum, id, eaque proident purus maxime nemo unde rhoncus, consectetur! Dignissim provident id? Consectetur possimus.
            </p>
            <p class="text">
              Nascetur egestas accusantium, nihil autem animi, unde animi cupidatat cupiditate! Congue adipisicing? Minim platea, repellendus facere, esse tempore. Quaerat nisi, placerat iusto magna faucibus, dictumst euismod tempus? Porro, fugiat ratione ullam vehicula erat quasi.
            </p>
            <picture class="image-block">
              <img src="multimedia/images/large/l-family-4.jpg" alt="">
            </picture>
            <p class="text">
              Vero conubia nostrud, viverra pharetra conubia reprehenderit! Incididunt magna elementum eius minus. Harum. Cras autem eius semper varius! Diam proin? Rem convallis rerum atque? Quis consectetuer purus commodo, quaerat facilis. Soluta diam? Mollis ut, taciti deserunt. Odit pretium! Voluptatum sollicitudin, sapiente curae! Maxime vel! Ipsum erat pariatur aperiam nisl ultrices consectetur! Turpis diam tempore, adipiscing animi tincidunt nemo, aute consectetur lacus mauris commodi.
            </p>
            <p class="text">
              Iure illum irure arcu netus, velit? Ducimus fuga tenetur ea tempus adipisci! Porta. Laboris taciti, augue condimentum sociosqu nobis aliquid! Dolorum habitasse condimentum perspiciatis quas numquam ullam quo esse accusantium quam. Phasellus diam veritatis metus quaerat! Consequatur voluptates? Porta cupiditate? Quibusdam nostra. Impedit blanditiis sed, metus euismod pede, ea nibh quod tempus perspiciatis iste, quas impedit, cumque donec qui senectus, porro porro per.
            </p>
          </div>
          <footer class="article-footer">
            <span class="action">Share this</span>
            <ul class="social-list">
              <li><a class="twitter" href="#"></a></li>
              <li><a class="facebook" href="#"></a></li>
              <li><a class="share" href="#"></a></li>
              <li><a class="email" href="#"></a></li>
              <li><a class="link" href="#"></a></li>
            </ul>
          </footer>
        </div>
      </article>
      <?php get_quote_banner() ?>
    </main>
    <?php get_top_footer() ?>
  </body>
</html>
