<?php require "../../functions.php"; ?>
<!DOCTYPE html>
<html lang="en-US">
  <head>
    <meta charset="utf-8">
    <meta name="author" content="This theme was created by the brazilian company DR Estúdio &ndash; https://danielrothier.com">
    <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1">

    <base href="../../" target="_self">
    <title>Reviews</title>

    <link rel="icon" href="multimedia/images/x-small/xs-favicon-1.jpg">

    <link rel="stylesheet" href="styles/reviews/reviews-min.css">
    <script defer src="scripts/reviews/reviews-min.js" type="module"></script>
  </head>
  <body>
    <?php get_top_header( array( 'Magazine' => '#', 'About' => 'content/about-us.php', 'Contact' => '#' ) ) ?>
    <main id="top-content">
      <?php get_intro_2( 'Reviews', 'Happy people talking about us' ) ?>
      <section id="testimonials">
        <div class="content-container">
          <div class="testimonial-content">
            <div class="square">
              <picture class="arrow previous">
                <img src="multimedia/images/x-small/xs-rarr-1.png" alt="">
              </picture>
            </div>
            <div class="slides-set">
              <div class="slide-block">
                <picture class="slideshow-image">
                  <img src="multimedia/images/medium/m-people-3.jpg" alt="">
                </picture>
                <div class="slideshow-text">
                  <header class="heading-container">
                    <h2 class="title">And they actually enjoyed it.</h2>
                  </header>
                  <div class="text-container">
                    <div class="text-set">
                      <q class="text">Awesome service! Great communication, easy to navigate website, even better pricing. Very happy</q>
                    </div>
                    <span class="author">Nora Vasconcellos</span>
                  </div>
                </div>
              </div>
              <div class="slide-block">
                <picture class="slideshow-image">
                  <img src="multimedia/images/medium/m-people-4.jpg" alt="">
                </picture>
                <div class="slideshow-text">
                  <header class="heading-container">
                    <h2 class="title">Great UX flow!</h2>
                  </header>
                  <div class="text-container">
                    <div class="text-set">
                      <q class="text">Awesome service! Great communication, easy to navigate website, even better pricing. Very happy</q>
                    </div>
                    <span class="author">Alexis Sablone</span>
                  </div>
                </div>
              </div>
            </div>
            <div class="square">
              <picture class="arrow next">
                <img src="multimedia/images/x-small/xs-rarr-1.png" alt="">
              </picture>
            </div>
          </div>
          <div hidden class="slideshow-content">
            <ul class="images">
              <li>
                <picture class="slideshow-image">
                  <img src="multimedia/images/medium/m-people-3.jpg" alt="">
                </picture>
              </li>
              <li>
                <picture class="slideshow-image">
                  <img src="multimedia/images/medium/m-people-4.jpg" alt="">
                </picture>
              </li>
              <li>
                <picture class="slideshow-image">
                  <img data-src="multimedia/images/medium/m-dolorum-1.jpg" alt="">
                </picture>
              </li>
              <li>
                <picture class="slideshow-image">
                  <img data-src="multimedia/images/medium/m-etiam-1.jpg" alt="">
                </picture>
              </li>
            </ul>
            <ul class="texts">
              <li>
                <div class="slideshow-text">
                  <header class="heading-container">
                    <h2 class="title">And they actually enjoyed it.</h2>
                  </header>
                  <div class="text-container">
                    <div class="text-set">
                      <q class="text">Awesome service! Great communication, easy to navigate website, even better pricing. Very happy</q>
                    </div>
                    <span class="author">Nora Vasconcellos</span>
                  </div>
                </div>
              </li>
              <li>
                <div class="slideshow-text">
                  <header class="heading-container">
                    <h2 class="title">Great UX flow!</h2>
                  </header>
                  <div class="text-container">
                    <div class="text-set">
                      <q class="text">Awesome service! Great communication, easy to navigate website, even better pricing. Very happy</q>
                    </div>
                    <span class="author">Alexis Sablone</span>
                  </div>
                </div>
              </li>
              <li>
                <div class="slideshow-text">
                  <header class="heading-container">
                    <h2 class="title">Nunc in turpis commodo.</h2>
                  </header>
                  <div class="text-container">
                    <div class="text-set">
                      <q class="text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse rutrum justo in suscipit convallis.</q>
                    </div>
                    <span class="author">Sample 1</span>
                  </div>
                </div>
              </li>
              <li>
                <div class="slideshow-text">
                  <header class="heading-container">
                    <h2 class="title">Phasellus lacinia ante vel dui mollis porttitor.</h2>
                  </header>
                  <div class="text-container">
                    <div class="text-set">
                      <q class="text">Aenean nec quam id ex eleifend aliquet vitae a erat. Duis egestas velit ultricies neque varius gravida.</q>
                    </div>
                    <span class="author">Sample 2</span>
                  </div>
                </div>
              </li>
            </ul>
          </div>
        </div>
      </section>
      <section id="assertion">
        <div class="content-container">
          <blockquote class="citation">
            <p class="message">
              Loved it! Pick a Policy is the best policy website! Very usefull
            </p>
            <span class="author">Chris Joslin</span>
          </blockquote>
        </div>
      </section>
      <?php get_quote_banner() ?>
    </main>
    <?php get_top_footer() ?>
  </body>
</html>
