<?php require "../../functions.php"; ?>
<!DOCTYPE html>
<html lang="en-US">
  <head>
    <meta charset="utf-8">
    <meta name="author" content="This theme was created by the brazilian company DR Estúdio &ndash; https://danielrothier.com">
    <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1">

    <base href="../../" target="_self">
    <title>FAQ</title>

    <link rel="icon" href="multimedia/images/x-small/xs-favicon-1.jpg">

    <link rel="stylesheet" href="styles/faq/faq-min.css">
    <script defer src="scripts/faq/_faq.js" type="module"></script>
  </head>
  <body>
    <?php get_top_header( array( 'Magazine' => '#', 'About' => 'content/about-us.php', 'Contact' => '#' ) ) ?>
    <main id="top-content">
      <?php get_intro_1( 'Common Questions', array( 'Questions' ) ) ?>
      <section id="common-questions">
        <h2 hidden>Most Common Questions</h2>
        <div class="content-container">
          <section class="question-block difference">
            <header class="heading-container">
              <h3 class="title">How are we different from other insurance sites?</h3>
            </header>
            <div class="text-set">
              <p class="text">
                First, a lot of insurance sites are lead generators—meaning they’ll capture your information (like a request for a quote) and sell that to multiple insurance brokers/agents who’ll try to sell you. We don’t do that. <strong>If you want insurance, we work with you personally.</strong> Second, most insurance sites don’t provide a great user experience. They don’t make it easy to comparison shop and understand what you’re buying.
              </p>
              <p class="text">
                The sites are poorly designed and filled with poorly written content. If they don’t confuse you to death, they bore you to death. That’s not how we do things. <strong>Everything we do is designed to deliver a great experience.</strong> We’ve devoted a lot of energy to designing an easy-to-navigate site, creating helpful tools and writing engaging content. After you give our site a spin, we think you’ll agree.
              </p>
            </div>
          </section>
          <section class="question-block money">
            <header class="heading-container">
              <h3 class="title">How do we make money?</h3>
            </header>
            <div class="text-set">
              <p class="text">
                We’re an insurance broker, so we get paid a commission by insurance companies for each sale. Insurance commissions are already baked into the price of an insurance policy, so you’re not paying any extra for using our service (or any broker’s service for that matter). We understand that getting a commission on a sale introduces a potential conflict of interest. But we don't push or give preference to any one insurer over another because of the commission. For example, we could make a lot more money selling whole life insurance instead of, or in addition to, term life insurance. But we don't think it's the right solution for most people who need life insurance. So we don't do it. And if you want to know what commission we'd earn on any insurance product you see on this site, just ask us. We'll be glad to tell you.
              </p>
            </div>
          </section>
          <section class="question-block license">
            <header class="heading-container">
              <h3 class="title">Are we licensed?</h3>
            </header>
            <div class="text-set">
              <p class="text">
                Yes — all insurance brokers are required by law to be licensed in each state they do business. You can find our licenses here.
              </p>
            </div>
          </section>
        </div>
      </section>
      <section id="questions-box">
        <div class="content-container">
          <header class="heading-container">
            <h2 class="title">Got a question?</h2>
          </header>
          <div class="questions-container">
            <article class="question displayed">
              <header class="question-body">
                <h3 class="question-text">
                  Beatae imperdiet quidem vivamus sed hac tempor mus consequat explicabo, velit morbi maxime nisl, harum. Est voluptatem veniam et nunc. Sapien molestiae similique netus?
                </h3>
                <picture class="circle">
                  <img src="multimedia/images/x-small/xs-rarr-2.png" alt="Show Answer"></img>
                </picture>
              </header>
              <main class="answer-body">
                <div class="text-set">
                  <p class="answer-text">
                    Senectus fermentum turpis accusamus deserunt nihil aliqua eu! Eiusmod delectus omnis eu, reiciendis lorem, iaculis. Irure parturient vehicula elementum sociosqu, scelerisque, tempore ullamco sed, alias? Exercitation fusce accusamus ipsa quo. Duis quibusdam libero in iure cursus, suscipit eum ante id nostra mollis vitae suscipit commodo sint. Aliquam ea aperiam feugiat adipiscing lacus, sunt ultrices! Adipiscing dictumst distinctio. Aptent corrupti voluptas.
                  </p>
                </div>
              </main>
            </article>
            <article class="question displayed">
              <header class="question-body">
                <h3 class="question-text">
                  Fusce mollis congue sapien vel dictum. Nulla viverra leo vitae justo tempus feugiat. Vestibulum ullamcorper suscipit lorem, vel tincidunt massa imperdiet a.
                </h3>
                <picture class="circle">
                  <img src="multimedia/images/x-small/xs-rarr-2.png" alt="Show Answer"></img>
                </picture>
              </header>
              <main class="answer-body">
                <div class="text-set">
                  <p class="answer-text">
                    Senectus fermentum turpis accusamus deserunt nihil aliqua eu! Eiusmod delectus omnis eu, reiciendis lorem, iaculis. Irure parturient vehicula elementum sociosqu, scelerisque, tempore ullamco sed, alias? Exercitation fusce accusamus ipsa quo. Duis quibusdam libero in iure cursus, suscipit eum ante id nostra mollis vitae suscipit commodo sint. Aliquam ea aperiam feugiat adipiscing lacus, sunt ultrices! Adipiscing dictumst distinctio. Aptent corrupti voluptas.
                  </p>
                </div>
              </main>
            </article>
            <article class="question displayed">
              <header class="question-body">
                <h3 class="question-text">
                  Mauris ut justo lobortis, aliquam ligula malesuada, dapibus ligula. Nam ac interdum risus, quis tempor erat.
                </h3>
                <picture class="circle">
                  <img src="multimedia/images/x-small/xs-rarr-2.png" alt="Show Answer"></img>
                </picture>
              </header>
              <main class="answer-body">
                <div class="text-set">
                  <p class="answer-text">
                    Senectus fermentum turpis accusamus deserunt nihil aliqua eu! Eiusmod delectus omnis eu, reiciendis lorem, iaculis. Irure parturient vehicula elementum sociosqu, scelerisque, tempore ullamco sed, alias? Exercitation fusce accusamus ipsa quo. Duis quibusdam libero in iure cursus, suscipit eum ante id nostra mollis vitae suscipit commodo sint. Aliquam ea aperiam feugiat adipiscing lacus, sunt ultrices! Adipiscing dictumst distinctio. Aptent corrupti voluptas.
                  </p>
                </div>
              </main>
            </article>
            <article class="question displayed">
              <header class="question-body">
                <h3 class="question-text">
                  Duis at sapien dictum, sodales tellus id, tempor velit. Sed leo leo, pulvinar eget viverra at, interdum vehicula elit.
                </h3>
                <picture class="circle">
                  <img src="multimedia/images/x-small/xs-rarr-2.png" alt="Show Answer"></img>
                </picture>
              </header>
              <main class="answer-body">
                <div class="text-set">
                  <p class="answer-text">
                    Senectus fermentum turpis accusamus deserunt nihil aliqua eu! Eiusmod delectus omnis eu, reiciendis lorem, iaculis. Irure parturient vehicula elementum sociosqu, scelerisque, tempore ullamco sed, alias? Exercitation fusce accusamus ipsa quo. Duis quibusdam libero in iure cursus, suscipit eum ante id nostra mollis vitae suscipit commodo sint. Aliquam ea aperiam feugiat adipiscing lacus, sunt ultrices! Adipiscing dictumst distinctio. Aptent corrupti voluptas.
                  </p>
                </div>
              </main>
            </article>
            <article class="question displayed">
              <header class="question-body">
                <h3 class="question-text">
                  Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Curabitur porttitor, nibh ullamcorper sollicitudin sodales, mi magna ultricies erat, nec dignissim justo elit sed leo.
                </h3>
                <picture class="circle">
                  <img src="multimedia/images/x-small/xs-rarr-2.png" alt="Show Answer"></img>
                </picture>
              </header>
              <main class="answer-body">
                <div class="text-set">
                  <p class="answer-text">
                    Senectus fermentum turpis accusamus deserunt nihil aliqua eu! Eiusmod delectus omnis eu, reiciendis lorem, iaculis. Irure parturient vehicula elementum sociosqu, scelerisque, tempore ullamco sed, alias? Exercitation fusce accusamus ipsa quo. Duis quibusdam libero in iure cursus, suscipit eum ante id nostra mollis vitae suscipit commodo sint. Aliquam ea aperiam feugiat adipiscing lacus, sunt ultrices! Adipiscing dictumst distinctio. Aptent corrupti voluptas.
                  </p>
                </div>
              </main>
            </article>
            <article class="question">
              <header class="question-body">
                <h3 class="question-text">
                  Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                </h3>
                <picture class="circle">
                  <img src="multimedia/images/x-small/xs-rarr-2.png" alt="Show Answer"></img>
                </picture>
              </header>
              <main class="answer-body">
                <div class="text-set">
                  <p class="answer-text">
                    Senectus fermentum turpis accusamus deserunt nihil aliqua eu! Eiusmod delectus omnis eu, reiciendis lorem, iaculis. Irure parturient vehicula elementum sociosqu, scelerisque, tempore ullamco sed, alias? Exercitation fusce accusamus ipsa quo. Duis quibusdam libero in iure cursus, suscipit eum ante id nostra mollis vitae suscipit commodo sint. Aliquam ea aperiam feugiat adipiscing lacus, sunt ultrices! Adipiscing dictumst distinctio. Aptent corrupti voluptas.
                  </p>
                </div>
              </main>
            </article>
            <article class="question">
              <header class="question-body">
                <h3 class="question-text">
                  Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
                </h3>
                <picture class="circle">
                  <img src="multimedia/images/x-small/xs-rarr-2.png" alt="Show Answer"></img>
                </picture>
              </header>
              <main class="answer-body">
                <div class="text-set">
                  <p class="answer-text">
                    Senectus fermentum turpis accusamus deserunt nihil aliqua eu! Eiusmod delectus omnis eu, reiciendis lorem, iaculis. Irure parturient vehicula elementum sociosqu, scelerisque, tempore ullamco sed, alias? Exercitation fusce accusamus ipsa quo. Duis quibusdam libero in iure cursus, suscipit eum ante id nostra mollis vitae suscipit commodo sint. Aliquam ea aperiam feugiat adipiscing lacus, sunt ultrices! Adipiscing dictumst distinctio. Aptent corrupti voluptas.
                  </p>
                </div>
              </main>
            </article>
          </div>
          <div class="buttons-container">
            <button type="button" name="next">
              <img src="multimedia/images/x-small/xs-rarr-1.png" alt="Previous">
            </button>
            <button type="button" name="previous">
              <img src="multimedia/images/x-small/xs-rarr-1.png" alt="Next">
            </button>
          </div>
        </div>
      </section>
      <?php get_quote_banner() ?>
    </main>
    <?php get_top_footer() ?>
  </body>
</html>
