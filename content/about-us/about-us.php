<?php require "../../functions.php"; ?>
<!DOCTYPE html>
<html lang="en-US">
  <head>
    <meta charset="utf-8">
    <meta name="author" content="This theme was created by the brazilian company DR Estúdio &ndash; https://danielrothier.com">
    <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1">

    <base href="../../" target="_self">
    <title>About us</title>

    <link rel="icon" href="multimedia/images/x-small/xs-favicon-1.jpg">

    <link rel="stylesheet" href="styles/about-us/about-us-min.css">
    <script defer src="scripts/about-us/about-us-min.js" type="module"></script>
  </head>
  <body>
    <?php get_top_header( array( 'Magazine' => '#', 'About' => 'content/about-us.php', 'Contact' => '#' ) ) ?>
    <main id="top-content">
      <?php get_intro_1( 'About Us' ) ?>
      <section id="explanations">
        <div class="content-container">
          <section class="starting-question">
            <picture class="side-image">
              <img src="multimedia/images/medium/m-family-3.jpg" alt="">
            </picture>
            <div class="text-container">
              <header class="heading-container">
                <h3 class="title">
                  Every business starts with a simple question.
                </h3>
              </header>
              <div class="text-set">
                <p class="text">
                  For the Pickapolicy founders, that question was: “Why is buying insurance such a frustrating experience? Frustrating is an understatement. Navigating the world of insurance is confusing, stressful and a step backward in time (hello, fax machines).
                  And even after they’ve bought a policy, people worry they might still be on the hook for something they missed.
                </p>
              </div>
            </div>
          </section>
          <section class="insurance-problem">
            <header class="heading-container">
              <h3 class="title">
                Let’s get real: insurance has a big consumer problem.
              </h3>
            </header>
            <div class="content-body">
              <picture class="side-image">
                <img src="multimedia/images/medium/m-people-2.jpg" alt="">
              </picture>
              <div class="text-set">
                <p class="text">
                  As consultants to the top insurance companies, we suspected that this consumer problem was behind a lot of things we observed.
                  It’s why life insurance ownership is at a 50-year low. It’s why health and disability under-insurance is behind most personal bankruptcies and home foreclosures. And it’s why we started Pickapolicy website.
                </p>
              </div>
            </div>
          </section>
        </div>
      </section>
      <section id="purpose">
        <div class="content-container">
          <header class="heading-container">
            <h2 class="title">
              <span class="side-line">Our mission is</span>
              <span class="main-line">
                To get people the insurance coverage <br>they need and make them feel good about it.
              </span>
            </h2>
          </header>
        </div>
      </section>
      <?php get_quote_banner() ?>
    </main>
    <?php get_top_footer() ?>
  </body>
</html>
