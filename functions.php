<?php
  function get_top_header( $pages )
    { include "templates/top-header.php"; };
  function get_top_footer()
    { include "templates/top-footer.php"; };
  function get_logo_img()
    { include "templates/logo-img.php"; };
  function get_social_list( $social_links )
    { include "templates/social-list.php"; };
  function get_quote_banner()
    { include "templates/quote-banner.php"; };
  function get_intro_1( $title, $lines = null )
    { include "templates/intro-1.php"; };
  function get_intro_2( $title, $subtitle, $side_image = null )
    { include "templates/intro-2.php"; };
?>
